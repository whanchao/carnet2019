package com.wkz.cn.jtparser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.wkz.cn.tools.MsgTools;
import com.wkz.cn.tools.NormalTools;


public class JtParserOf808 {
	public static void main(String[] args) {
		System.out.println(getFormatMsg("7e0608403401000000000012222320901934020000000101530da90003027f2c7806f5e3310276b25506ed690e20062400000020063000000000813a00630006486168613031ff7e"));
	}
	/**
	 * 将协议数据解析为格式化字符串
	 * @param protocolData
	 * @return
	 * @throws Exception
	 */
	public static String getFormatMsg(String protocolData){		
		//消息预处理：去空格、转义还原、去除首尾标识
		String dataWith7e = MsgTools.decTM(NormalTools.getStrWithOutSpace(protocolData));
		String rawData = dataWith7e.substring(2, dataWith7e.length() - 2);

		int offset = 0;
		// 消息ID
		String msgId = "0x" + rawData.substring(offset, offset += 4);
		// 消息体属性
		int msgProperty = NormalTools.hexStr2Int(rawData.substring(offset, offset += 4));
		int isNewVersion = (msgProperty & 0x4000) >> 14;
		int version = isNewVersion == 0 ? -1: NormalTools.hexStr2Int(rawData.substring(offset, offset += 2));
		int isSubPackage = (msgProperty & 0x2000) >> 13;
		int isEnc = (msgProperty & 0x1c00) >> 10;
		int bodyLength = (msgProperty & 0x3FFF);
		// 终端手机号
		String telephone = version == -1 ?rawData.substring(offset, offset += 12): rawData.substring(offset, offset += 20);
		// 消息流水号
		int msgSn = NormalTools.hexStr2Int(rawData.substring(offset, offset += 4));
		
		// 原生消息体
		String msgBody = rawData.substring(rawData.length() - bodyLength * 2 - 2, rawData.length() - 2);
		
		// 生成最终的格式化字符串
		String formatMsg = JtParserOf808.getFormatMsgHeader(msgId, version, isSubPackage, isEnc, telephone, msgSn)
						+ JtParserOf808.getFormatMsgBody(msgId, msgBody, version);
		
		return formatMsg;
	}
	
	/**
	 * 获取格式化消息头数据
	 * @param msgId
	 * @param telephone
	 * @param msgSn
	 * @return
	 */
	public static String getFormatMsgHeader(String msgId, int version, int isSubPackage, int isEnc, String telephone, int msgSn){
		String formatMsgHeader = "\n【" + msgId + "】"
				+ MsgTools.msgDict.get(msgId)
				+ "\n协议版本：" + (version > -1 ? version: "旧版2011")
				+ "\n是否分包：" + (isSubPackage == 1 ? "分包": "不分包")
				+ "\n加密方式：" + (isEnc == 1 ? "RSA加密": "不加密")
				+ "\n终端手机号：" + NormalTools.removeStartZeros(telephone)
				+ "\n消息流水号：" + msgSn
				+ "\n\n";
		return formatMsgHeader;
	}
	
	/**
	 * 获取格式化消息体数据
	 * @param msgId
	 * @param msgBody
	 * @return
	 */
	public static String getFormatMsgBody(String msgId, String msgBody, int version){
		Map<String, Object> msgBodyMap = getMsgBodyMap(msgId, msgBody, version);
		return NormalTools.getFormatJson(JSON.toJSONString(msgBodyMap));
	}
	
	
	/**
	 * 根据msgId将消息体解析为Map<String, Object>
	 * @param msgId
	 * @param msgBody
	 * @return
	 * @throws Exception
	 */
	private static Map<String, Object> getMsgBodyMap(String msgId, String msgBody, int version){
		Map<String, Object> msgBodyMap = null; 
		switch(msgId){
			case "0x8001":
				msgBodyMap = parse0x8001(msgBody, version);	break;
			case "0x0002":
				msgBodyMap = parse0x0002(msgBody, version); break;
			case "0x8107":
				msgBodyMap = parse0x8107(msgBody, version); break;
			case "0x0100":
				msgBodyMap = parse0x0100(msgBody, version);	break;
			case "0x8100":
				msgBodyMap = parse0x8100(msgBody, version);	break;
			case "0x0102":
				msgBodyMap = parse0x0102(msgBody, version);	break;
			case "0x0200":
				msgBodyMap = parse0x0200(msgBody, version);	break;
			case "0x0608":
				msgBodyMap = parse0x0608(msgBody, version); break;
			default:
				msgBodyMap = new HashMap<>();
				msgBodyMap.put("Error", "该协解析功能暂不支持！！！");
		}
		return msgBodyMap;
	}

	
	/**
	 * 【0x8001】平台通用应答消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x8001(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		int offset = 0;
		msgBodyInfo.put("serialNo", NormalTools.hexStr2Int(msgBody.substring(offset,  offset += 4)));
		String reqMsgId = msgBody.substring(offset, offset += 4);
		msgBodyInfo.put("reqMsgId", reqMsgId);
		msgBodyInfo.put("reqMsgDesc", MsgTools.msgDict.get("0x" + reqMsgId));
		String result = msgBody.substring(offset, offset += 2);
		String resultStr = "";
		switch(result) {
			case "00": resultStr = "成功/确认"; break;
			case "01": resultStr = "失败"; break;
			case "02": resultStr = "消息有误"; break;
			case "03": resultStr = "不支持"; break;
			case "04": resultStr = "报警处理确认"; break;
		}
		msgBodyInfo.put("result", resultStr);
		return msgBodyInfo;
	}
	
	/**
	 * 【0x0002】终端心跳
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x0002(String msgBody, int version){
		return getEmptyMsgBodyMap();
	}
	
	/**
	 * 【0x8107】查询终端属性消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x8107(String msgBody, int version){
		return getEmptyMsgBodyMap();
	}
	
	
	/**
	 * 【0x0100】终端注册消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x0100(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		int offset = 0;
		msgBodyInfo.put("provinceId", msgBody.subSequence(offset, offset += 4));
		msgBodyInfo.put("cityId", msgBody.substring(offset, offset += 4));
		// 2011->2019字段长度有变化
		if(version < 0) {
			msgBodyInfo.put("manufacturerId", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 10)).trim());			// BYTE[5]
			msgBodyInfo.put("teminalType", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 40)).trim());				// BYTE[20]
			msgBodyInfo.put("teminalId", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 14)).trim());				// BYTE[7]
		}else {
			msgBodyInfo.put("manufacturerId", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 22)).trim());			// BYTE[11]
			msgBodyInfo.put("teminalType", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 60)).trim());				// BYTE[30]
			msgBodyInfo.put("teminalId", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 60)).trim());				// BYTE[30]
		}
		msgBodyInfo.put("carPlateColor", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));
		msgBodyInfo.put("VIN", NormalTools.hexStr2Str(msgBody.substring(offset, msgBody.length())));
		
		return msgBodyInfo;
	}
	
	/**
	 * 【0x8100】终端注册应答消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x8100(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		int offset = 0;
		msgBodyInfo.put("respSerialNo", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));
		String registerResult = msgBody.substring(offset, offset += 2);
		String registerResultStr = "";
		switch(registerResult) {
			case "00": registerResultStr = "成功"; break;
			case "01": registerResultStr = "车辆已被注册"; break;
			case "02": registerResultStr = "数据库中无该车辆"; break;
			case "03": registerResultStr = "终端已被注册"; break;
			case "04": registerResultStr = "数据库中无该终端"; break;
		}
		msgBodyInfo.put("registerResult", registerResultStr);
		msgBodyInfo.put("authenticationCode", NormalTools.hexStr2Str(msgBody.substring(offset, msgBody.length())));
		return msgBodyInfo;
	}
	
	/**
	 * 【0x0102】终端鉴权消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x0102(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		// 2011版只有一个鉴权码
		if(version < 0) {
			msgBodyInfo.put("authenticationCode", NormalTools.hexStr2Str(msgBody));			
		}else {
			int offset = 0;
			int authCodeLength = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2));
			msgBodyInfo.put("authCodeLength", authCodeLength);															// 鉴权码长度
			msgBodyInfo.put("authCode", NormalTools.hexStr2Str(msgBody.substring(offset, offset += authCodeLength)));	// 鉴权码内容
			msgBodyInfo.put("teminalIMEI", NormalTools.hexStr2Str(msgBody).substring(offset, offset += 30).trim());		// 终端IMEI
			msgBodyInfo.put("softwareVersion", NormalTools.hexStr2Str(msgBody.substring(offset, offset += 40)).trim());	// 软件版本号
		}
		return msgBodyInfo;
	}
	
	/**
	 * 【0x0200】位置信息汇报消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x0200(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		int offset = 0;
		
		msgBodyInfo.put("alarmIdentify", msgBody.substring(offset, offset += 8));
		msgBodyInfo.put("status", msgBody.substring(offset, offset += 8));
		msgBodyInfo.put("lat", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8))/1000000.0);
		msgBodyInfo.put("lng", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8))/1000000.0);
		msgBodyInfo.put("height", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));
		msgBodyInfo.put("speed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4))/10.0);
		msgBodyInfo.put("direction", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));
		msgBodyInfo.put("dateTime", handleTime("20" + msgBody.substring(offset, offset += 12)));
		
		List<Map<String, Object>> locationAppendInfoList = new ArrayList<>();
		while(offset < msgBody.length()){
			Map<String, Object> locationAppendInfo = new HashMap<>();
			locationAppendInfo.put("locAppendInfoId", msgBody.substring(offset, offset += 2));
			locationAppendInfo.put("locAppendInfoLength", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));
			int locAppendInfoLength = (int)locationAppendInfo.get("locAppendInfoLength");
			locationAppendInfo.put("locAppendInfoContent", msgBody.substring(offset, offset += locAppendInfoLength * 2));
			
			locationAppendInfoList.add(handleLocAppendInfo(locationAppendInfo));
		}
		msgBodyInfo.put("locationAppendInfoList", locationAppendInfoList);
		
		return msgBodyInfo;
	}
	
	
	/**
	 * 【0x0608】位置信息汇报消息解析
	 * @param msgBody
	 * @return
	 */
	private static Map<String, Object> parse0x0608(String msgBody, int version){
		Map<String, Object> msgBodyInfo = new HashMap<>();
		int offset = 0;
		int areaType = Integer.valueOf(msgBody.substring(offset, offset += 2));
		msgBodyInfo.put("queryType", areaType);												// 区域类型
		msgBodyInfo.put("dataCount", Integer.valueOf(msgBody.substring(offset, offset += 8)));	// 区域数据条数
		msgBodyInfo.put("areaDataList", getAreaDataList(msgBody, offset, areaType));			// 区域数据列表

		return msgBodyInfo;
	}
	private static List<Map<String, Object>> getAreaDataList(String msgBody, int offset, int queryType){
		switch(queryType) {
			case 1:
				return getType1or2AreaDataList(msgBody, offset, 1);
			case 2:
				return getType1or2AreaDataList(msgBody, offset, 2);
			case 3:
				return getType3AreaDataList(msgBody, offset);
			case 4:
				return getType4AreaDataList(msgBody, offset);
			default:
				return null;
		}
	}
	// 获取圆形或矩形区域数据列表
	private static List<Map<String, Object>> getType1or2AreaDataList(String msgBody, int offset, int areaType){
		List<Map<String, Object>> list = new ArrayList<>();
		while(offset < msgBody.length()) {
			Map<String, Object> areaData = new HashMap<String, Object>();
			areaData.put("areaId", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 区域ID(4)
			int areaProperty = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaProperty", areaProperty);												// 区域属性(2)
			areaData.put("lat1", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 纬度
			areaData.put("lng1", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 经度
			if(areaType == 1) {
				areaData.put("radius", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));
			}else {
				areaData.put("lat2", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 纬度
				areaData.put("lng2", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 经度
			}
			if((areaProperty & 1) > 0) {
				areaData.put("startTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 开始时间
				areaData.put("endTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 结束时间
			}
			if((areaProperty & 2) > 0) {
				areaData.put("maxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));		// 最高速度
				areaData.put("overSpeedTime", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));	// 超速持续时间
				areaData.put("nightMaxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));	// 夜间最高速度
			}
			int len = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaNameLength", len);																	// 区域名称长度
			areaData.put("areaName", NormalTools.hexStr2Str(msgBody.substring(offset, offset += len * 2)).trim());	// 区域名称
			list.add(areaData);
		}
		return list;
	}
	// 获取多边形区域数据列表
	private static List<Map<String, Object>> getType3AreaDataList(String msgBody, int offset){
		List<Map<String, Object>> list = new ArrayList<>();
		while(offset < msgBody.length()) {
			Map<String, Object> areaData = new HashMap<String, Object>();
			areaData.put("areaId", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 区域ID(4)
			int areaProperty = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaProperty", areaProperty);												// 区域属性(2)
			if((areaProperty & 1) > 0) {
				areaData.put("startTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 开始时间
				areaData.put("endTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 结束时间
			}
			if((areaProperty & 2) > 0) {
				areaData.put("maxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));		// 最高速度
				areaData.put("overSpeedTime", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));	// 超速持续时间
			}
			int peakCount = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));			// 多边形区域顶点数
			areaData.put("peakCount", peakCount);
			
			List<String> peakList = new ArrayList<>();
			int endIdx = offset + peakCount * 16;
			while(offset < endIdx) {
				String latLng = msgBody.substring(offset, offset += 16);
				peakList.add( NormalTools.hexStr2Int(latLng.substring(8, 16))/1000000.0 + ","
							+ NormalTools.hexStr2Int(latLng.substring(0, 8))/1000000.0);
			}
			areaData.put("peakDataList", peakList);													// 多边形区域的顶点项数据
			
			if((areaProperty & 2) > 0) {
				areaData.put("nightMaxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));	// 夜间最高速度
			}
			int len = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaNameLength", len);																	// 区域名称长度
			areaData.put("areaName", NormalTools.hexStr2Str(msgBody.substring(offset, offset += len * 2)).trim());	// 区域名称
			list.add(areaData);
		}
		return list;
	}
	// 获取线路型区域数据列表
	private static List<Map<String, Object>> getType4AreaDataList(String msgBody, int offset){
		List<Map<String, Object>> list = new ArrayList<>();
		while(offset < msgBody.length()) {
			Map<String, Object> areaData = new HashMap<String, Object>();
			areaData.put("areaId", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));	// 路线ID(4)
			int areaProperty = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaProperty", areaProperty);												// 路线属性(2)
			if((areaProperty & 1) > 0) {
				areaData.put("startTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 开始时间
				areaData.put("endTime", handleTime("20" + msgBody.substring(offset, offset += 12)));		// 结束时间
			}
			
			int lineNodeCount = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("lineNodeCount", lineNodeCount);								// 路线总拐点数
			Object[] lineNodeInfo = getLineNodeInfo(msgBody, offset, lineNodeCount);	
			areaData.put("lineNodeList", lineNodeInfo[0]);									// 路线拐点数据列表
			
			offset = (Integer)(lineNodeInfo[1]);
			int len = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4));
			areaData.put("areaNameLength", len);																	// 区域名称长度
			areaData.put("areaName", NormalTools.hexStr2Str(msgBody.substring(offset, offset += len * 2)).trim());	// 区域名称
			list.add(areaData);
		}
		return list;
	}
	
	private static Object[] getLineNodeInfo(String msgBody, int offset, int lineNodeCount) {
		List<Map<String, Object>> nodeDataList = new ArrayList<>();
		for(int i = 0; i < lineNodeCount; i ++) {
			Map<String, Object> nodeData = new HashMap<>();
			nodeData.put("nodeId", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));		// 拐点ID
			nodeData.put("roadId", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8)));		// 路段ID
			nodeData.put("nodeLat", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8))/1000000.0);		// 拐点纬度
			nodeData.put("nodeLng", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 8))/1000000.0);		// 拐点经度
			nodeData.put("roadWidth", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));			// 路段宽度
			int roadProperty = NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2));
			nodeData.put("roadProperty", roadProperty);		// 路段属性
			// 行驶时长信息
			if((roadProperty & 1) > 0) {
				nodeData.put("driveTimeUpThreshold", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));	//时间过长阈值
				nodeData.put("driveTimeDownThreshold", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));	//时间不足阈值
			}
			// 超速信息
			if((roadProperty & 2) > 0) {
				nodeData.put("maxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));		// 最高速度
				nodeData.put("overSpeedTime", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 2)));	// 超速持续时间
				nodeData.put("nightMaxSpeed", NormalTools.hexStr2Int(msgBody.substring(offset, offset += 4)));	// 夜间最高速度
			}
			nodeDataList.add(nodeData);
		}
		return new Object[] {nodeDataList, offset};
	}
	
	
	// 获取空消息体Map
	private static Map<String, Object> getEmptyMsgBodyMap(){
		Map<String, Object> emptyMap = new HashMap<>();
		emptyMap.put("Content", "此消息的消息体为空！");
		return emptyMap;
	}
	
	/**
	 * 位置附加信息处理：非苏标，以及苏标中的通用信息未解析
	 * @param locAppendInfo
	 * @return
	 */
	private static Map<String, Object> handleLocAppendInfo(Map<String, Object> appendInfo){
		String appendInfoId = appendInfo.get("locAppendInfoId").toString();
		String appendInfoContent = appendInfo.get("locAppendInfoContent").toString();
		
		int offset = 0;
		Map<String, Object> appendInfoContentMap = new HashMap<>();
		switch (appendInfoId){
			case "64":
				appendInfoContentMap.put("alarmId", appendInfoContent.substring(offset, offset += 8));
				appendInfoContentMap.put("alarmStatus", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("alarmEventType", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("alarmGrade", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("frontCarSpeed", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 2)));
				appendInfoContentMap.put("frontCarDistance", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 2)));
				appendInfoContentMap.put("deviateType", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("roadSignType", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("roadSignValue", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 2)));
				appendInfo.put("locAppendInfoContent", appendInfoContentMap);
				break;
			case "65":
				appendInfoContentMap.put("alarmId", appendInfoContent.substring(offset, offset += 8));
				appendInfoContentMap.put("alarmStatus", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("alarmEventType", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("alarmGrade", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("tiredLevel", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 2)));
				appendInfo.put("locAppendInfoContent", appendInfoContentMap);
				break;
			case "66":
				appendInfoContentMap.put("alarmId", appendInfoContent.substring(offset, offset += 8));
				appendInfoContentMap.put("alarmStatus", appendInfoContent.substring(offset, offset += 2));
				offset += 70;	// 跳过告警通用信息
				
				appendInfoContentMap.put("alarmEventCount", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 2)));
				List<Map<String, Object>> alarmEventList = new ArrayList<>();
				while(offset < appendInfoContent.length()){
					Map<String, Object> alarmEvent = new HashMap<>();
					alarmEvent.put("TyreLocation", appendInfoContent.substring(offset, offset += 2));
					alarmEvent.put("TyreEventType", appendInfoContent.substring(offset, offset += 4));
					alarmEvent.put("TyreAirPressure", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 4)));
					alarmEvent.put("TyreTemperature", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 4)));
					alarmEvent.put("batteryElectric", NormalTools.hexStr2Int(appendInfoContent.substring(offset, offset += 4)) + "%");
					alarmEventList.add(alarmEvent);
				}
				appendInfoContentMap.put("alarmEventList", alarmEventList);
				appendInfo.put("locAppendInfoContent", appendInfoContentMap);
				break;
			case "67":
				appendInfoContentMap.put("alarmId", appendInfoContent.substring(offset, offset += 8));
				appendInfoContentMap.put("alarmStatus", appendInfoContent.substring(offset, offset += 2));
				appendInfoContentMap.put("alarmEventType", appendInfoContent.substring(offset, offset += 2));
				appendInfo.put("locAppendInfoContent", appendInfoContentMap);
				break;
		}
		
		return appendInfo;
	}
	

    private static String handleTime(String timeStr) {
    	return timeStr.substring(0, 4) + "-" + timeStr.substring(4, 6) + "-" + timeStr.substring(6, 8) + " "
    		+ timeStr.substring(8, 10) + ":" + timeStr.substring(10, 12) + ":" + timeStr.substring(12, 14);
    }
}
