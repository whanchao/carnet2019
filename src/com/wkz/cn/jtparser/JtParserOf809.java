package com.wkz.cn.jtparser;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.wkz.cn.tools.MsgTools;
import com.wkz.cn.tools.NormalTools;
import com.wkz.cn.tools.SuperiorInfo;


public class JtParserOf809 {	
	private static Map<String, String> colorDict = getColorDict();
	
	public static void main(String[] args) {
		System.out.println(JtParserOf809.getFormatMsg(""));
	}
	
	/**
	 * 将协议数据解析为格式化字符串
	 * 子业务类型ID分三种情况：
	 * 		1）链路管理相关，没有子业务类型标识；
	 * 		2）平台信息交换，子业务类型标识在第0-2位；
	 * 		3）车辆信息相关，子业务类型标识在第22-24位；
	 * @param bodyData
	 * @return
	 * @throws Exception
	 */
	public static String getFormatMsg(String protocolData){
		// 去空格转义，去除首尾标识
		String dataWithoutSpace = MsgTools.decTM809(NormalTools.getStrWithOutSpace(protocolData));
		String rawData = dataWithoutSpace.substring(2, dataWithoutSpace.length()-2);
		
		// 判断消息体长度是否正确
		int msgLength = NormalTools.hexStr2Int(rawData.substring(0, 8));
		if(msgLength != dataWithoutSpace.length()/2)
			return "Jt809消息解析失败：消息头中的消息长度为{" +  msgLength + "}，与消息实际长度{" + dataWithoutSpace.length()/2 + "}不符！";
		
		// 解析消息头
		int msgSn = NormalTools.hexStr2Int(rawData.substring(8, 16));
		String msgId = "0x" + rawData.substring(16, 20);
		int accessCode = NormalTools.hexStr2Int(rawData.substring(20, 28));
		String version = "v" + NormalTools.hexStr2Int(rawData.substring(28, 30))
							+ "." + NormalTools.hexStr2Int(rawData.substring(30, 32))
							+ "." + NormalTools.hexStr2Int(rawData.substring(32, 34));
		String encryptFlag = rawData.substring(34, 36);
		int encryptKey = NormalTools.hexStr2Int(rawData.substring(36, 44));
		
		// 解析消息体
		String msgBody = rawData.substring(44, rawData.length()-4);
		if (encryptFlag.equals("01")) {
			SuperiorInfo superior = MsgTools.getSuperior(accessCode);
			if(superior == null)
				return "Jt809消息解析失败：上级平台接入码【" + accessCode + "】在数据库中不存在！";
			msgBody = MsgTools.encrypt809(superior.getEncrypt_m1(), superior.getEncrypt_a1(), superior.getEncrypt_c1(), superior.getEncrypt_key(), msgBody);
		} else if (!encryptFlag.equals("00")) {
			return "Jt809消息解析失败：消息中的加密标识错误，既不是0也不是1！";
		}
		
		// 存在子业务类型标识，取子业务类型标识
		if(msgId.endsWith("00")) {
			if(msgId.startsWith("0x13") || msgId.startsWith("0x93")) {
				msgId = "0x" + msgBody.substring(0, 4);			// 平台间信息交互类，子业务类型ID位于前2位；
			}else {
				msgId = "0x" + msgBody.substring(44, 48);		// 车辆相关信息交互类，子业务类型位于第22-24位
			}
		}
		String formatMsgHeader = getFormatMsgHeader(msgSn, msgId, accessCode, version, encryptFlag, encryptKey);
		String formatMsgBody = getFormatMsgBody(msgId, msgBody);
		
		return formatMsgHeader + formatMsgBody;
	}
	
	
	private static String getFormatMsgHeader(int msgSn, String msgId, int accessCode, String version, 
			String encryptFlag, int encryptKey) {
		String encrypt = encryptFlag.equals("01")? "加密KEY：" + encryptKey: "未加密";
		String formatHeader = "\n【" + msgId + "】" + MsgTools.msgDict.get(msgId) + "(" + encrypt + ")"
								+ "\n协议版本" + version + "，流水号：" + msgSn + "，平台接入码：" + accessCode
								+ "\n-----------------------------------------------------------\n";
		return formatHeader;
	}
	private static String getFormatMsgBody(String msgId, String msgBody) {
		switch(msgId) {
			case "0x1001": return get0x1001Data(msgBody) + "\n";
			case "0x1002": return get0x1002Data(msgBody) + "\n";
			case "0x1005": return "主链路连接保持请求，消息体为空！\n";
			case "0x1006": return "主链路连接保持应答，消息体为空！\n";
			case "0x9001": return get0x9001Data(msgBody) + "\n";
			case "0x9002": return get0x9002Data(msgBody) + "\n";
			case "0x9005": return "从链路连接保持请求，消息体为空！\n";
			case "0x9006": return "从链路连接保持应答，消息体为空！\n";
			case "0x1201": return get0x1201Data(msgBody) + "\n";
			case "0x1202": return get0x1202Data(msgBody) + "\n";
			case "0x9301": return get0x9301Data(msgBody) + "\n";
			case "0x1301": return get0x1301Data(msgBody) + "\n";
			case "0x9302": return get0x9302Data(msgBody) + "\n";
			case "0x1302": return get0x1302Data(msgBody) + "\n";
			case "0x9401": return get0x9401Data(msgBody) + "\n";
			case "0x1401": return get0x1401Data(msgBody) + "\n";
		}
		return "此协议暂不支持！";
	}
	

	/**
	 * 获取【0x1201 车辆注册信息】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1201Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("plateNumber", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 21 * 2)).trim());
		resultMap.put("plateColor", colorDict.get(bodyData.substring(offset, offset += 1 * 2)));
		resultMap.put("bizType", "0x" + bodyData.substring(offset, offset += 2 * 2));
		offset += 4 * 2;	// 后续数据长度
		resultMap.put("platformId", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 11 * 2)).trim());
		resultMap.put("producerId", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 11 * 2)).trim());
		resultMap.put("teminalType", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 20 * 2)).trim());
		resultMap.put("teminalId", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 7 * 2)).trim());
		resultMap.put("simCard", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 12 * 2)).substring(1, 12));
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x1202 实时位置信息】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1202Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("plateNumber", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 21 * 2)).trim());
		resultMap.put("plateColor", colorDict.get(bodyData.substring(offset, offset += 1 * 2)));
		resultMap.put("bizType", "0x" + bodyData.substring(offset, offset += 2 * 2));
		offset += 4 * 2; 	// 后续数据长度
		resultMap.put("isEncrypt", bodyData.substring(offset, offset += 1 * 2));	// 是否加密
		resultMap.put("dateTime", get809DateTime(bodyData.substring(offset, offset += 7 * 2)));
		resultMap.put("lng", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("lat", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("speed", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 2 * 2)));
		resultMap.put("recorderSpeed", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 2 * 2)));
		resultMap.put("totalMileage", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("direction", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 2 * 2)));
		resultMap.put("hight", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 2 * 2)));
		resultMap.put("carState", bodyData.substring(offset, offset += 4));
		resultMap.put("carAlarm", bodyData.substring(offset, offset += 4));
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x1001 主链路登录请求】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1001Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("userId", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("password", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 8 * 2)).trim());
		resultMap.put("lowLevelIp", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 32 * 2)).trim());
		resultMap.put("lowLevelPort", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 2 * 2)));
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x1002 主链路登录应答】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1002Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		
		String result = bodyData.substring(0, 1 * 2);
		String resultStr = "";
		switch(result) {
			case "00": resultStr = "成功"; break;
			case "01": resultStr = "IP地址不正确"; break;
			case "02": resultStr = "接入码不正确"; break;
			case "03": resultStr = "用户没有注册"; break;
			case "04": resultStr = "密码错误"; break;
			case "05": resultStr = "资源紧张，稍后再连接（已经占用）"; break;
			case "06": resultStr = "其他"; break;
			default:
				resultStr = "协议解析出错，result[" + result + "]不符合协议约定！";
		}
		
		resultMap.put("result", resultStr);
		resultMap.put("verifyCode", NormalTools.hexStr2Int(bodyData.substring(2, 10)));
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x9001 从链路连接请求】
	 * @param bodyData
	 * @return
	 */
	private static String get0x9001Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put("verifyCode", NormalTools.hexStr2Int(bodyData));
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x9002 从链路连接应答】
	 * @param bodyData
	 * @return
	 */
	private static String get0x9002Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		String resultStr = "";
		switch(bodyData) {
			case "00": resultStr = "成功"; break;
			case "01": resultStr = "VERIFY_CODE错误"; break;
			case "02": resultStr = "资源紧张，稍后再连接（已经占用）"; break;
			case "03": resultStr = "其他"; break;
			default:
				resultStr = "协议解析出错，result[" + bodyData + "]不符合协议约定！";
		}
		resultMap.put("result", resultStr);
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x9301 从链路平台查岗请求】
	 * @param bodyData
	 * @return
	 */
	private static String get0x9301Data(String bodyData){
		return sendDownMsgBodyParse(bodyData);
	}
	
	/**
	 * 获取【0x1301 从链路平台查岗应答】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1301Data(String bodyData){
		return sendDownMsgBodyParse(bodyData);
	}
	
	
	/**
	 * 获取【0x9302 从链路下发平台间报文请求】
	 * @param bodyData
	 * @return
	 */
	private static String get0x9302Data(String bodyData){
		return sendDownMsgBodyParse(bodyData);
	}
	
	/**
	 * 获取【0x1302 从链路下发平台间报文应答】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1302Data(String bodyData){
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("msgId", bodyData.substring(offset, offset += 2 * 2));
		int surplusLength = NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2));
		if(surplusLength != 4) 
			return "协议解析出错，消息体剩余数据长度[" + surplusLength + "]与实际剩余长度[4]不符！";

		resultMap.put("infoId", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}

	/**
	 * 获取【0x9401 从链路报警督办请求】
	 * @param bodyData
	 * @return
	 */
	private static String get0x9401Data(String bodyData) {
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("carPlate", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 21 * 2)).trim());
		resultMap.put("carColor", colorDict.get(bodyData.substring(offset, offset += 1 * 2)));
		resultMap.put("msgId", "0x" + bodyData.substring(offset, offset += 2 * 2));
		int surplusLength = NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2));
		if(surplusLength != bodyData.length()/2 - 28)
			return "协议解析出错，消息体剩余数据长度[" + surplusLength + "]与实际剩余长度[" + (bodyData.length()/2 - 28) + "]不符！";
		String warnSrc = bodyData.substring(offset, offset += 1 * 2);
		String warnSrcStr = "";
		switch(warnSrc) {
			case "01": warnSrcStr = "车载终端"; break;
			case "02": warnSrcStr = "企业监控平台"; break;
			case "03": warnSrcStr = "政府监管平台"; break;
			case "09": warnSrcStr = "其他"; break;
		}
		resultMap.put("warnSrc", warnSrcStr);
		resultMap.put("warnType", bodyData.substring(offset, offset += 2 * 2));	// 报警类型（Jt809协议-表75）
		resultMap.put("warnTime", get809DateTime(bodyData.substring(offset, offset += 8 * 2)));
		resultMap.put("supervisionId", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("supervisionEndTime", get809DateTime(bodyData.substring(offset, offset += 8 * 2)));
		resultMap.put("supervisionLevel", bodyData.substring(offset, offset += 1 * 2).equals("00")? "紧急": "一般");
		resultMap.put("supervisor", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 16 * 2)).trim());
		resultMap.put("supervisorTel", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 20 * 2)).trim());
		resultMap.put("supervisorEmail", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 30 * 2)).trim());
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	/**
	 * 获取【0x1401 从链路报警督办应答-主链路回复】
	 * @param bodyData
	 * @return
	 */
	private static String get0x1401Data(String bodyData) {
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("carPlate", NormalTools.hexStr2Str(bodyData.substring(offset, offset += 21 * 2)).trim());
		resultMap.put("carColor", colorDict.get(bodyData.substring(offset, offset += 1 * 2)));
		resultMap.put("msgId", "0x" + bodyData.substring(offset, offset += 2 * 2));
		int surplusLength = NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2));
		if(surplusLength != bodyData.length()/2 - 28)
			return "协议解析出错，消息体剩余数据长度[" + surplusLength + "]与实际剩余长度[" + (bodyData.length()/2 - 28) + "]不符！";
		resultMap.put("supervisionId", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		String result = bodyData.substring(offset, offset += 1 * 2);
		String resultStr = "";
		switch(result) {
			case "00": resultStr = "处理中"; break;
			case "01": resultStr = "已处理完毕"; break;
			case "02": resultStr = "不做处理"; break;
			case "03": resultStr = "将来处理"; break;
		}
		resultMap.put("result", resultStr);
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}	

	/** 
	 * 查岗请求0x9301、查岗应答0x1301、下发平台间报文请求0x9302，消息体格式完全相同
	 * @param bodyData
	 * @return
	 */
	private static String sendDownMsgBodyParse(String bodyData) {
		Map<String, Object> resultMap = new HashMap<>();
		
		int offset = 0;
		resultMap.put("msgId", "0x" + bodyData.substring(offset, offset += 2 * 2));
		int surplusLength = NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2));
		if(surplusLength != bodyData.length()/2 - 6) {
			return "协议解析出错，消息体剩余数据长度[" + surplusLength + "]与实际剩余长度[" + (bodyData.length()/2 - 6) + "]不符！";
		}
		
		//JT_809-2011补充协议增加了[下级对象类型]和[下级对象ID]两个字段
		String objectType = bodyData.substring(offset, offset += 1 * 2);
		String objectTypeStr = "";
		switch(objectType) {
			case "01": objectTypeStr = "当前连接的下级平台"; break;
			case "02": objectTypeStr = "下级平台所属单一业户"; break;
			case "03": objectTypeStr = "下级平台所属所有业户"; break;
		}
		resultMap.put("objectType", objectTypeStr);
		String objectId = bodyData.substring(offset, offset += 12 * 2);
		if(!objectType.equals("03"))
			objectId = NormalTools.hexStr2Str(objectId).trim();
		resultMap.put("objectId", objectId);
		
		resultMap.put("infoId", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("infoLength", NormalTools.hexStr2Int(bodyData.substring(offset, offset += 4 * 2)));
		resultMap.put("infoContent", NormalTools.hexStr2Str(bodyData.substring(offset, bodyData.length())).trim());
		
		return NormalTools.getFormatJson(JSON.toJSONString(resultMap));
	}
	
	// 809位置上报中的日期时间解析
	private static String get809DateTime(String rawStr){
		int offset = 0;
		int day = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 1 * 2));
		int month = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 1 * 2));
		int year = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 2 * 2));
		
		int hour = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 1 * 2));
		int minute = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 1 * 2));
		int second = NormalTools.hexStr2Int(rawStr.substring(offset, offset += 1 * 2));
		
		return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
		
	}
	
	// 车牌号颜色DICT
	private static Map<String, String> getColorDict(){
		Map<String, String> colorMap = new HashMap<>();
		colorMap.put("01", "蓝色");
		colorMap.put("02", "黄色");
		colorMap.put("03", "黑色");
		colorMap.put("04", "白色");
		colorMap.put("05", "农黄");
		colorMap.put("06", "农蓝");
		colorMap.put("07", "农绿");
		colorMap.put("09", "其他");
		return colorMap;
	}
}
