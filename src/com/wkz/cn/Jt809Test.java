package com.wkz.cn;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.wkz.cn.tools.MsgTools;
import com.wkz.cn.tools.NormalTools;

public class Jt809Test {
	public static Integer accessCode = 21000070;
	public static Integer verifyCode = 16843009;
	
	public static void main(String[] args) throws Exception{
		Socket socket = new Socket("127.0.0.1", 12345);
		for(int i = 0; i < 10; i ++) {
			connAndSend(socket, Jt809Test.get0x9301Data("平台查岗！！！！！"));
			Thread.sleep(1000);
		}
		
		
//		System.out.println("主链路登录应答：" + NormalTools.getStrWithSpace(Jt809Test.get0x1002Data()));
//		System.out.println("从链路连接：" + NormalTools.getStrWithSpace(Jt809Test.get0x9001Data()));
//		System.out.println("--------------------------------------------------------------------------");
//		System.out.println("链路连接及消息传输情况上报请求：" + NormalTools.getStrWithSpace(Jt809Test.get0x9102Data()));
//		System.out.println("下发报警预警信息：" + NormalTools.getStrWithSpace(Jt809Test.get0x9402Data("京F12345", "打雷了，下雨了，快收衣服了！！！")));
//		System.out.println("平台查岗：" + NormalTools.getStrWithSpace(Jt809Test.get0x9301Data("查岗查岗，天王盖地虎... ...")));
//		System.out.println("下发平台间报文：" + NormalTools.getStrWithSpace(Jt809Test.get0x9302Data("下发平台间报文：好好工作，升职加薪指日可待！")));
		
//		String[] alarmTypes = new String[] {
//				"0001", "0002", "0003", "0004", "0005", "0008", "0009", "000a", "000b", "000c", "000d",
//				"0010", "0011", "0012", "0013", "0014", "00ff",
//				"a001", "a002", "a003", "a004", "a005", "a006", "a007", "a008", "a009", "a00a", "a00b", "a00c", "a00f"
//		};
//		
//		for(String alarmType: alarmTypes) {
//			connAndSend(socket, Jt809Test.get0x9402Data("京F12345", "【0x" + alarmType + "】打雷了！下雨了！快来收衣服啊！！！", alarmType));	
//			Thread.sleep(100);
//		}
//		
//		socket.close();
	}
	
	public static void connAndSend(Socket socket, String protocolData) throws Exception{
		OutputStream os = socket.getOutputStream();
		InputStream is = socket.getInputStream();
		os.write(NormalTools.hex2Bin(Jt809Test.get0x9001Data()));
		os.flush();
		
		byte[] buff = new byte[256];
		is.read(buff);
		System.out.println("从链路连接：" + NormalTools.getStrWithSpace(NormalTools.bin2Hex(buff).split("5d")[0] + "5d"));
		
		os.write(NormalTools.hex2Bin(protocolData));
		os.flush();	
	}
	
	public static void testProtocol() {
		System.out.println("主链路登录应答：" + NormalTools.getStrWithSpace(Jt809Test.get0x1002Data()));
		System.out.println("从链路连接：" + NormalTools.getStrWithSpace(Jt809Test.get0x9001Data()));
		System.out.println("从链路注销：" + NormalTools.getStrWithSpace(Jt809Test.get0x9003Data()));
		System.out.println("从链路连接保持：" + NormalTools.getStrWithSpace(Jt809Test.get0x9005Data()));
		System.out.println("从链路断开通知：" + NormalTools.getStrWithSpace(Jt809Test.get0x9007Data()));
		System.out.println("上级主动关闭链路：" + NormalTools.getStrWithSpace(Jt809Test.get0x9008Data()));
		System.out.println("链路连接及消息传输情况上报请求：" + NormalTools.getStrWithSpace(Jt809Test.get0x9102Data()));
		System.out.println("下发平台间消息序列号通知消息：" + NormalTools.getStrWithSpace(Jt809Test.get0x9103Data()));		
		System.out.println("平台查岗：" + NormalTools.getStrWithSpace(Jt809Test.get0x9301Data("查岗查岗，天王盖地虎... ...")));
		System.out.println("下发平台间报文：" + NormalTools.getStrWithSpace(Jt809Test.get0x9302Data("下发平台间报文：好好工作，升职加薪指日可待！")));
		System.out.println("消息补传-按序列号：" + NormalTools.getStrWithSpace(Jt809Test.get0x9303Data("1202", 5, 7)));
		System.out.println("消息补传-按起始UTC：" + NormalTools.getStrWithSpace(Jt809Test.get0x9303Data("1202", NormalTools.get809UTCHex(-3600 * 5), 5)));
	}
	
	
	/**
	 * 【0x1002 主链路登录应答消息】
	 */
	public static String get0x1002Data() {
		String msgBody = "00"									// 登录成功
						+ NormalTools.int2Hex(verifyCode, 4);	// 返给企业平台的校验码
		return MsgTools.get809Message("1002", accessCode, msgBody);
	}
	
	/**
	 * 【0x9001 从链路连接请求消息】， 企业平台用0x9002进行应答
	 */
	public static String get0x9001Data() {
		return MsgTools.get809Message("9001", accessCode, NormalTools.int2Hex(verifyCode, 4));
	}

	/**
	 * 【0x9003 从链路注销请求消息】， 企业平台用0x9004进行应答
	 */
	public static String get0x9003Data() {
		return MsgTools.get809Message("9003", accessCode, NormalTools.int2Hex(verifyCode, 4));
	}
	
	/**
	 * 【0x9005 从链路连接保持请求消息】， 企业平台用0x9006进行应答
	 */
	public static String get0x9005Data() {
		return MsgTools.get809Message("9005", accessCode, "");
	}
	
	/**
	 * 【0x9007 从链路断开通知消息】
	 */
	public static String get0x9007Data() {
		return MsgTools.get809Message("9007", accessCode, "00");
	}
	
	/**
	 * 【0x9008 上级平台主动关闭链路通知消息】
	 */
	public static String get0x9008Data() {
		return MsgTools.get809Message("9008", accessCode, "ff");
	}
	
	/**
	 * 【0x9102 平台链路连接情况与车辆定位消息传输情况上报请求】企业平台用0x1102应答
	 */
	public static String get0x9102Data() {
		String msgBody = "9102"							// 子业务类型ID
						+ NormalTools.int2Hex(27, 4)	// 后续数据长度
						+ "0102030405060708090010"					// PLATFORM_ID：行政区划代码+平台编号
						+ NormalTools.get809UTCHex(-3600 * 24 * 365)		// 开始时间
						+ NormalTools.get809UTCHex(3600 * 24);				// 结束时间
		return MsgTools.get809Message("9102", accessCode, msgBody);
	}
	
	/**
	 * 【0x9103 下发平台间消息序列号通知消息】业务流程应该先平台发0x1103，上级再发0x9103
	 */
	public static String get0x9103Data() {
		int sn = 0;
		String[] subMsgIds = getAllSubMsgIds();
		String datas = "";
		for(String subMsgId: subMsgIds) {
			datas += subMsgId + NormalTools.int2Hex(sn) + NormalTools.get809UTCHex();
		}
		String msgBody = "9103"													// 子业务类型标识
						+ NormalTools.int2Hex(subMsgIds.length * 14 + 1, 4)		// 后续数据长度
						+ NormalTools.int2Hex(subMsgIds.length, 1)				// 返回的子业务类型标识序列号总数
						+ datas;												// 数据项列表
		return MsgTools.get809Message("9103", accessCode, msgBody);
	}
	
	/**
	 * 【0x9301 平台查岗】，企业平台用[0x1301平台查岗应答]来响应
	 */
	public static String get0x9301Data(String info) {
		String infoHex = NormalTools.str2Hex(info);
		int infoLength = infoHex.length() / 2;
		
		String msgBody = "9301"											// 业务类型标识(2)
					   + NormalTools.int2Hex(30 + infoLength, 4)		// 后续数据长度(4)
					   + "01"												// 查岗对象类型(1)
					   //+ NormalTools.fillZero("", 20)						// 查岗对象ID(20)
					   + NormalTools.fillZeroAtTail(NormalTools.str2Hex("thisIsObjectId"), 20)
					   + "7F"												// 查岗应答时限(1)
					   + "00000007"										// 信息ID(4)
					   + NormalTools.int2Hex(infoLength, 4)				// 信息长度(4)
					   + infoHex;										// 信息内容
		
		return MsgTools.get809Message("9300", accessCode, msgBody);
	}
	
	/**
	 * 【0x9302 下发平台间报文】，企业平台用[0x1302下发平台间报文应答]来响应
	 */
	public static String get0x9302Data(String message) {
		String infoHex = NormalTools.str2Hex(message);
		int infoLength = infoHex.length() / 2;
		
		String msgBody = "9302"											// 业务类型标识(2)
					   + NormalTools.int2Hex(30 + infoLength, 4)		// 后续数据长度(4)
					   + "01"												// 下发报文的对象类型(1)
					   + NormalTools.fillZero("", 20)						// 下发报文的对象ID(20)
					   + "00000007"										// 信息ID(4)
					   + NormalTools.int2Hex(infoLength, 4)				// 信息长度(4)
					   + infoHex;										// 信息内容
		
		return MsgTools.get809Message("9300", accessCode, msgBody);
	}
	
	/**
	 * 【0x9303 下发平台间消息补传请求消息】，企业平台与之对应的是[0x1301上传平台间消息补传请求]
	 */
	public static String get0x9303Data(String subMsgId, int startSn, int endSn) {
		String msgBody = "9303"										// 子业务类型ID
						+ NormalTools.int2Hex(19, 4)				// 后续数据长度
						+ subMsgId									// 需要重传消息的子业务类型标识
						+ NormalTools.int2Hex(endSn-startSn+1, 1)	// 重传消息总数
						+ NormalTools.int2Hex(startSn, 4)		// 起始报文序列号
						+ NormalTools.int2Hex(endSn, 4)			// 结束报文序列号
						+ NormalTools.fillZero("", 8);			// 重传的起始utc时间：使用起始序列号的方式，此处全为0
		return MsgTools.get809Message("9300", accessCode, msgBody);
	}
	public static String get0x9303Data(String subMsgId, String startUtcTime, int count) {
		String msgBody = "9303"										// 子业务类型ID
						+ NormalTools.int2Hex(19, 4)				// 后续数据长度
						+ subMsgId									// 需要重传消息的子业务类型标识
						+ NormalTools.int2Hex(count, 1)		// 重传消息总数
						+ NormalTools.fillZero("", 4)					// 起始报文序列号：使用起始utc时间，此处全为0
						+ NormalTools.fillZero("", 4)					// 结束报文序列号：使用起始utc时间，此处全为0
						+ startUtcTime;						// 重传的起始utc时间
		return MsgTools.get809Message("9300", accessCode, msgBody);
	}
	
	
	/**
	 * 【0x9401 上级督办】未进行2019版测试
	 */
	public static String get0x9401Data(
						String plateNum, int plateColor, 
						int alarmSrc, int alarmType, long alarmTime, 
						long supEndTime, int supLevel, String supvisor, String supTel, String supEmail) {
		
		String msgId = "9401";
		String msgBody = NormalTools.str2Hex(plateNum, 21)		// 车牌号(21)
					   + NormalTools.int2Hex(plateColor, 1)		// 车牌号颜色(1)
					   + msgId								// 业务数据类型(2)
					   + NormalTools.int2Hex(92, 4)				// 后续数据长度(4)
					   + NormalTools.int2Hex(alarmSrc, 1)			// 报警信息来源(1)
					   + NormalTools.int2Hex(alarmType)			// 报警类型(2)
					   + NormalTools.long2Hex(alarmTime)			// 报警时间(8)
					   + "00000000"							// 督办ID(4)
					   + NormalTools.long2Hex(supEndTime)			// 督办截止时间(8)
					   + NormalTools.int2Hex(supLevel, 1)			// 督办级别(1)
					   + NormalTools.str2Hex(supvisor, 16)		// 督办人(16)
					   + NormalTools.str2Hex(supTel, 20)			// 督办人Tel(20)
					   + NormalTools.str2Hex(supEmail, 30);		// 督办人Email(30)：补充协议长度为32
				
		return MsgTools.get809Message(msgId, accessCode, msgBody);
	}
	/**
	 * 【0x9402 下发报警预警信息】
	 * @return
	 */
	public static String get0x9402Data(String carPlate, String alarmDesc, String alarmType) {
		String carPlateHex = NormalTools.fillZeroAtTail(NormalTools.str2Hex(carPlate), 21);				// 车牌号长度21，不知道为什么！！！
		String alarmDescHex = NormalTools.str2Hex(alarmDesc);
		String msgBody = "9402"									// 子业务类型标识
						+ NormalTools.int2Hex(carPlateHex.length() / 2 + alarmDescHex.length() / 2 + 57, 4)		// 后续数据长度
						+ "0102030405060708096666"			// PLATFORM_ID——发起平台唯一编码
						+ alarmType							// 报警类型：0x0010-违规行驶报警
						+ NormalTools.get809UTCHex()		// 报警时间
						+ NormalTools.get809UTCHex(- 20 * 60)		// 事件开始时间
						+ NormalTools.get809UTCHex(- 10 * 60)		// 事件结束时间
						+ carPlateHex				// 车牌号码
						+ "02"						// 车牌颜色
						+ "0102030405060708098888"			// PLATFORM_ID——被报警平台唯一编码
						+ NormalTools.int2Hex(666, 4)		// 驾驶线路ID
						+ NormalTools.int2Hex(alarmDescHex.length() / 2, 4)		// 报警信息内容长度
						+ alarmDescHex;											// 报警信息内容
				
		return MsgTools.get809Message("9400", accessCode, msgBody);
	}
	
	public static String[] getAllSubMsgIds() {
		String[] subMsgIds = new String[] {
				"1201", "1202", "1203", "1205", "1206", "1207", "1208", "1209", "120a", "120b", "120c", "120d", "120e",
				"9201", "9202", "9203", "9204", "9205", "9206", "9207", "9208", "9209", "920a", "920b", "920c", "920d",
				"1301", "1302", "1303", "9301", "9302", "9303",
				"1401", "1402", "1403", "1411", "1412", "1413", "9401", "9402", "9403", 
				"1501", "1502", "1503", "1504", "1505", "9501", "9502", "9503", "9504", "9505", 
				"1601", "1602", "9601"
		};
		return subMsgIds;
	}
	
	public static void send(Socket socket, String msg) throws Exception{
		OutputStream os = socket.getOutputStream();
		os.write(NormalTools.hex2Bin(msg));
		os.flush();
	}
}
