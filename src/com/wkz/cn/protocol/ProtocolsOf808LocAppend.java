package com.wkz.cn.protocol;

import java.util.Random;

import com.wkz.cn.tools.NormalTools;

/**
 * 【位置附加信息】
 * 【0x01】里程，DWORD(4字节)，1/10km，对应车上里程表读数
 * 【0x02】油量，WORD(2字节)，1/10L，对应车上油量表读数
 * 【0x03】速度，WORD(2字节)，1/10km/h，行驶记录功能获取的速度
 * 【0x04】事件ID，WORD(2字节)，需要人工确认报警事件的ID(从1开始计数)
 * ========================================================================================
 * 【0x11*】超速报警信息，1或5字节(区域ID占4个字节)：0无：1圆形：2矩形：3多边形：4路线
 * 【0x12*】进出区域报警信息，BYTE[6](6字节)：0进1出 || 1圆形：2矩形：3多边形：4路线
 * 【0x13*】路段行驶时间不足/过长报警，BYTE[7](7字节)：路段ID || 行驶时间 || 0不足1过长
 * ------------------------------------------------------------------------------------
 * 【0x14*】视频相关报警，DWORD(4字节)
 * bit0信号丢失，bit1信号遮挡，bit2存储器故障，bit3其他设备故障，bit4客车超员，bit5异常驾驶，bit6录像存储达到阈值
 * 【0x15**】视频信号丢失报警状态，DWORD(4字节)，32位表示32个逻辑通道
 * 【0x16**】视频信号遮挡报警状态，DWORD(4字节)，32位表示32个逻辑通道
 * 【0x17**】视频存储器故障报警状态，WORD(2字节)，12个主存储+4个灾备存储
 * 【0x18**】异常驾驶行为报警，WORD(2字节)，前3个bit：疲劳、打电话、抽烟
 * ========================================================================================
 * 【0x25】扩展车辆信号状态位，DWORD(4字节)
 * 【0x2A】IO状态位，WORD(2字节)
 * 【0x2B】模拟量，DWORD(4字节)bit0-15：AD0，bit16-31：AD1
 * 【0x30】无线通信信号强度，BYTE(1字节)
 * 【0x31】GNSS定位卫星数量，BYTE(1字节）
 * 【0xE0】后续自定义信息长度，BYTE(1字节)，暂无设为0；
 * @author Administrator
 */

public class ProtocolsOf808LocAppend {
	public static Random random = new Random();

	/**
	 * 获取符合warnSign描述的完整位置附加信息
	 * @param warnSign
	 * @return
	 */
	public static String getAllLocAppendInfo(int warnSign){
		String locAppendInfo = "";
		locAppendInfo = ProtocolsOf808LocAppend.getMileage()		// 里程
						+ ProtocolsOf808LocAppend.getOilMass()		// 油量
						+ ProtocolsOf808LocAppend.getSpeed()		// 速度
						+ ProtocolsOf808LocAppend.getEventId()							// 需人工确认的报警事件ID
						+ ProtocolsOf808LocAppend.getCarriageTemperature()				// 车厢温度[2019新增字段]
						+ ProtocolsOf808LocAppend.getWarnLocAppendInfo(warnSign, 0)	// 报警相关 --- 共3个
						//+ ProtocolsOfLocAppend.getVideoAlarmInfo(127)				// 1078视频报警相关 --- 共5个
						+ ProtocolsOf808LocAppend.getExCarSignState()			// 扩展车辆信号状态位
						+ ProtocolsOf808LocAppend.getIOState()					// IO状态位
						+ ProtocolsOf808LocAppend.getSimulateQuantity()		// 模拟量
						+ ProtocolsOf808LocAppend.getWireSignalStrength()		// 无线通信信号强度
						+ ProtocolsOf808LocAppend.getGnssLocSatelliteCount();	// GNSS定位卫星数量
		return locAppendInfo;
	}
	
	/**
	 * 获取符合warnSign和videoWarnSign描述的【报警相关的】位置附件信息
	 * @param warnSign,videoWarnSign
	 * @return
	 */
	public static String getWarnLocAppendInfo(int warnSign){
		return getWarnLocAppendInfo(warnSign, 127);
	}
	public static String getWarnLocAppendInfo(int warnSign, int videoWarnSign){
		String locAppendInfo = "";
		if(NormalTools.isFlagOne(warnSign, 1))
			locAppendInfo += ProtocolsOf808LocAppend.getOverSpeedAlarm();								// 超速报警详细描述
		if(NormalTools.isFlagOne(warnSign, 16))
			locAppendInfo += ProtocolsOf808LocAppend.getTirePressure();								// 胎压报警信息
		if(NormalTools.isFlagOne(warnSign, 20))
			locAppendInfo += ProtocolsOf808LocAppend.getIOAreaAlarmInfo(random.nextInt(3) + 1);		// 进出区域详细描述
		if(NormalTools.isFlagOne(warnSign, 21))
			locAppendInfo += ProtocolsOf808LocAppend.getIOAreaAlarmInfo(4);							// 进出路线详细描述
		if(NormalTools.isFlagOne(warnSign, 22))
			locAppendInfo += ProtocolsOf808LocAppend.getWayDriverTimeAlarm();							// 路段行驶时长不足/过长详细描述
		if(videoWarnSign > 0)
			locAppendInfo += ProtocolsOf808LocAppend.getVideoAlarmInfo(videoWarnSign);					// 视频相关报警附加信息
		return locAppendInfo;
	}
		
	
	/**
	 * 【0x01】里程，DWORD(4字节)，1/10km，对应车上里程表读数
	 * @return
	 */
	public static String getMileage(){
		return getMileage(random.nextInt(32768));
	}
	public static String getMileage(int mileage){
		System.out.println("里程：" + mileage);
		return "0104" + NormalTools.int2Hex(mileage, 4);
	}
	
	/**
	 * 【0x02】油量，WORD(2字节)，1/10L，对应车上油量表读数
	 * @return
	 */
	public static String getOilMass(){
		return getOilMass(random.nextInt(32768));
	}
	public static String getOilMass(int oilMass){
		System.out.println("油量：" + oilMass);
		return "0202" + NormalTools.int2Hex(oilMass);
	}
	
	/**
	 * 【0x03】速度，WORD(2字节)，1/10km/h，行驶记录功能获取的速度
	 * @return
	 */
	public static String getSpeed(){
		return getSpeed(random.nextInt(32768));
	}
	public static String getSpeed(int speed){
		System.out.println("速度：" + speed);
		return "0302" + NormalTools.int2Hex(speed);
	}
	
	/**
	 * 【0x04】事件ID，WORD(2字节)，需要人工确认报警事件的ID(从1开始计数)
	 * @return
	 */
	public static String getEventId(){
		return getEventId(random.nextInt(32768));
	}
	public static String getEventId(int eventId){
		System.out.println("需人工确认报警事件ID：" + eventId);
		return "0402" + NormalTools.int2Hex(eventId);
	}
	
	/**
	 * 【0x05】胎压：2019版新增
	 */
	public static String getTirePressure() {
		return getTirePressure(8);
	}
	public static String getTirePressure(int tyreCount) {
		String data = "";
		for(int i = 0; i < tyreCount; i ++)
			data += NormalTools.int2Hex(10000 + random.nextInt(100));
		for(int i = tyreCount * 2; i < 30; i ++)
			data += "ff";
		return "051e" + data;
	}
	
	/**
	 * 【0x06】车厢温度：2019版新增
	 */
	public static String getCarriageTemperature() {
		return getCarriageTemperature(NormalTools.getRandomInt(-100, 100));
	}
	public static String getCarriageTemperature(int temperature) {
		System.out.println("车厢温度：" + temperature);
		return "0602" + NormalTools.int2Hex(temperature);
	}
	
	/**
	 * 【0x11】超速报警信息，1或5字节(区域ID占4个字节)：0无：1圆形：2矩形：3多边形：4路线
	 * @return
	 */
	public static String getOverSpeedAlarm(){
		return getOverSpeedAlarm(random.nextInt(5));
	}
	public static String getOverSpeedAlarm(int zero2four){
		if(zero2four == 0){
			System.out.println("超速报警：无特定路段/区域");
			return "110100";
		}else{
			int roadId = random.nextInt(100000) + 10000;
			String areaType = zero2four == 1?"圆形区域": zero2four == 2?"矩形区域" : zero2four == 3?"多边形区域" : "路段";
			System.out.println("超速报警-" + areaType + "：" + roadId);
			return "1105" + "0" + zero2four + NormalTools.int2Hex(roadId, 4);
		}
	}
	
	/**
	 * 【0x12】进出区域报警信息，BYTE[6](6字节)
	 * @return
	 */
	public static String getIOAreaAlarmInfo(){
		return getIOAreaAlarmInfo(random.nextInt(4) + 1);
	}
	public static String getIOAreaAlarmInfo(int oneTofour){
		return getIOAreaAlarmInfo(random.nextInt(2), oneTofour);
	}
	public static String getIOAreaAlarmInfo(int ioType, int oneTofour){
		String io = ioType == 0?"驶入-":"驶出-";
		int areaId = random.nextInt(100000) + 10000;
		String areaType = oneTofour == 1?"圆形区域": oneTofour == 2?"矩形区域" : oneTofour == 3?"多边形区域" : "路段";
		System.out.println(io + areaType + "：" + areaId);
		return "1206" + "0" + oneTofour + NormalTools.int2Hex(areaId, 4) + "0" + ioType;
	}
	
	/**
	 * 【0x13】路段行驶时间不足/过长报警，BYTE[7](7字节)
	 * @return
	 */
	public static String getWayDriverTimeAlarm(){
		return getWayDriverTimeAlarm(random.nextInt(2));
	}
	public static String getWayDriverTimeAlarm(int result){
		return getWayDriverTimeAlarm(result, random.nextInt(500) + 120);
	}
	public static String getWayDriverTimeAlarm(int result, int driverTime){
		String rltStr = result == 0?"不足":"过长";
		int roadId = random.nextInt(100000) + 10000;
		System.out.println("路段" + roadId + "：行驶时间" + rltStr + "：时长" + driverTime);
		return "1307" + NormalTools.int2Hex(roadId, 4) + NormalTools.int2Hex(driverTime) + "0" + result;
	}
	
	//------------------------ 【1078】视频相关报警：START ----------------------//
	/**
	 * 【0x14】视频相关报警，DWORD(4字节)：
	 *  -------------------------------------------------------------
	 * 	bit0信号丢失，bit1信号遮挡，bit2存储器故障，bit3其他设备故障，bit4客车超员，bit5异常驾驶，bit6录像存储达到阈值
	 * 【0x15】视频信号丢失报警状态，DWORD(4字节)，32位表示32个逻辑通道
	 * 【0x16】视频信号遮挡报警状态，DWORD(4字节)，32位表示32个逻辑通道
	 * 【0x17】视频存储器故障报警状态，WORD(2字节)，12个主存储+4个灾备存储
	 * 【0x18】异常驾驶行为报警，WORD(2字节)，前3个bit：疲劳、打电话、抽烟
	 * @return
	 */
	public static String getVideoAlarmInfo(){
		int videoAlarmSign = random.nextInt(128);
		return getVideoAlarmInfo(videoAlarmSign);
	}
	public static String getVideoAlarmInfo(int videoAlarmSign){
		System.out.println("视频相关报警：" + NormalTools.getSign(videoAlarmSign) + "（" + videoAlarmSign + "）");
		String videoAlarmInfo = "1404" + NormalTools.int2Hex(videoAlarmSign, 4);
		String videoAlarmAppendInfo = "";
		
		// 信号丢失、遮挡，存储故障及异常驾驶的详细描述
		if(NormalTools.isFlagOne(videoAlarmSign, 0))
			videoAlarmAppendInfo += getVideoSignalLossState();		// {0x15}视频信号丢失：bit0-bit31共32个逻辑通道
		if(NormalTools.isFlagOne(videoAlarmSign, 1))
			videoAlarmAppendInfo += getVideoSignalBlockState();		// {0x16}视频信号遮挡：bit0-bit31共32个逻辑通道
		if(NormalTools.isFlagOne(videoAlarmSign, 2))
			videoAlarmAppendInfo += getStorageFaultState();			// {0x17}存储器故障：bit0-bit11为12个主存储，bit12-bit15为4个灾备存储
		if(NormalTools.isFlagOne(videoAlarmSign, 5))
			videoAlarmAppendInfo += getAbnormalDriveInfo();			// {0x18}异常驾驶行为：bit0疲劳，bit1打电话，bit2抽烟
				
		return videoAlarmInfo + videoAlarmAppendInfo;
	}
	
	/**
	 * 【0x15】视频信号丢失报警状态，DWORD(4字节)，32位表示32个逻辑通道
	 * @return
	 */
	public static String getVideoSignalLossState(){
		int signalLossState = random.nextInt(Integer.MAX_VALUE);
		System.out.println("视频信号丢失报警状态：" + NormalTools.getSign(signalLossState) + "（" + signalLossState + "）");
		return "1504" + NormalTools.int2Hex(signalLossState, 4);
	}
	
	/**
	 * 【0x16】视频信号遮挡报警状态，DWORD(4字节)，32位表示32个逻辑通道
	 * @return
	 */
	public static String getVideoSignalBlockState(){
		int signalBlockState = random.nextInt(Integer.MAX_VALUE);
		System.out.println("视频信号遮挡状态：" + NormalTools.getSign(signalBlockState) + "（" + signalBlockState + "）");
		return "1604" + NormalTools.int2Hex(signalBlockState, 4);
	}
	
	/**
	 * 【0x17】视频存储器故障报警状态，WORD(2字节)，12个主存储+4个灾备存储
	 * @return
	 */
	public static String getStorageFaultState(){
		int storageFaultState = random.nextInt(65536);
		System.out.println("视频存储器故障状态：" + NormalTools.getSign(storageFaultState) + "（" + storageFaultState + "）");		
		return "1702" + NormalTools.int2Hex(storageFaultState);
	}
	
	/**
	 * 【0x18】异常驾驶行为报警，WORD(2字节)，前3个bit：疲劳、打电话、抽烟
	 * @return
	 */
	public static String getAbnormalDriveInfo(){
		int abnormalDriverType = random.nextInt(8);
		System.out.println("异常驾驶类型：" + NormalTools.getSign(abnormalDriverType) + "（" + abnormalDriverType + "）");
		return "1802" + NormalTools.int2Hex(abnormalDriverType);
	}
	//------------------------- 【1078】视频相关报警：END -----------------------//
	
	
	
	/** 扩展车辆信号状态位-详细说明
	 * bit0-近光灯信号，bit1-远光灯信号，bit2-右转向灯信号，bit3-左转向灯信号，bit4-制动信号，bit5-倒挡信号
	 * bit6-雾灯信号，bit7-示廓灯，bit8-喇叭信号，bit9-空调状态，bit10-空挡信号
	 * bit11-缓速器工作，bit12-ABS工作，13-加热器工作，14-离合器状态
	 */
	
	/**
	 * 【0x25】扩展车辆信号状态位，DWORD(4字节)
	 * @return
	 */
	public static String getExCarSignState(){
		int state = random.nextInt(32768);
		System.out.println("扩展车辆信号状态：" + NormalTools.getSign(state) + "（" + state + "）");
		return "2504" + NormalTools.int2Hex(state, 4);
	}
	
	/**
	 * 【0x2A】IO状态位，WORD(2字节)
	 * @return
	 */
	public static String getIOState(){
		int ioState = random.nextInt(2) + 1;
		String ioStr = ioState == 1?"深度休眠状态":"休眠状态";
		System.out.println("IO状态为：" + ioStr);
		return "2a02" + NormalTools.int2Hex(ioState);
	}
	
	/**
	 * 【0x2B】模拟量，DWORD(4字节)bit0-15：AD0，bit16-31：AD1
	 * @return
	 */
	public static String getSimulateQuantity(){
		int ad0 = random.nextInt(65536);
		int ad1 = random.nextInt(65536);
		System.out.println("模拟量\tAD0：" + ad0 + "；AD1：" +ad1);
		return "2b04" + NormalTools.int2Hex(ad1) + NormalTools.int2Hex(ad0);		// 先高位后地位，即AD1 AD0
	}
	
	/**
	 * 【0x30】无线通信信号强度，BYTE(1字节)
	 * @return
	 */
	public static String getWireSignalStrength(){
		int signStrength = random.nextInt(256);
		System.out.println("无线通信信号强度：" + signStrength);
		return "3001" + NormalTools.int2Hex(signStrength, 1);
	}
	
	/**
	 * 【0x31】GNSS定位卫星数量，BYTE(1字节)
	 * @return
	 */
	public static String getGnssLocSatelliteCount(){
		int sateCount = random.nextInt(256);
		System.out.println("GNSS定位卫星数量：" + sateCount);
		return "3101" + NormalTools.int2Hex(sateCount, 1);
	}
	
	/**
	 * 【0xE0】后续自定义信息长度，BYTE(1字节)，暂无设为0；
	 * @return
	 */
	public static String getFollowUpInfoLength(){
		return "E000";
	}
	
	/**
	 * 生成2的整次幂
	 * @param num
	 * @return
	 */
	@SuppressWarnings("unused")
	private static int pow(int num){
		return 1 << num;
	}
}
