package com.wkz.cn.protocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.wkz.cn.tools.MsgTools;
import com.wkz.cn.tools.NormalTools;

public class ProtocolsOf808 {	
	private static Random random = new Random();
	
	/** -------------------- 808 基础协议 -------------------- */
	
	/**
	 *  【0x0100注册信息】兼容2011与2019
	 */
	public static String get0x0100Data(String teminalTel, String carPlate) {
		return get0x0100Data(teminalTel, carPlate, true);
	}
	public static String get0x0100Data(String teminalTel, String carPlate, boolean isNewVersion){
		String msgId = "0100";
		String msgBody = "";
		if(isNewVersion) {
			msgBody = NormalTools.int2Hex(13)									//省域ID
					+ NormalTools.int2Hex(100)									//市县域ID
					+ NormalTools.fillZero(NormalTools.str2Hex("NB007"), 11)	//制造商ID（11）[2019->长度]
					+ NormalTools.fillZero(NormalTools.str2Hex("DV05"), 30)		//终端型号（30）[2019->长度]
					+ NormalTools.fillZero(NormalTools.str2Hex("2700888"), 30)	//终端ID（30）[2019->长度]
					+ "01"														//车牌颜色：1蓝2黄3黑4白5绿9其他[2019->增5]
					+ NormalTools.fillZero(NormalTools.str2Hex(carPlate), 8);	//车牌号（8）			
		}else {
			msgBody = NormalTools.int2Hex(13)									//省域ID
					+ NormalTools.int2Hex(100)									//市县域ID
					+ NormalTools.fillZero(NormalTools.str2Hex("NB007"), 5)	//制造商ID（5）
					+ NormalTools.fillZero(NormalTools.str2Hex("DV05"), 20)		//终端型号（20）
					+ NormalTools.fillZero(NormalTools.str2Hex("2700888"), 7)	//终端ID（7）
					+ "01"														//车牌颜色：1蓝2黄3黑4白5绿9其他[2019->增5]
					+ NormalTools.fillZero(NormalTools.str2Hex(carPlate), 8);	//车牌号（8）
		}
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0102鉴权信息】兼容2011与2019
	 */
	public static String get0x0102Data(String teminalTel, String authCode) {
		return get0x0102Data(teminalTel, authCode, true);
	}
	public static String get0x0102Data(String teminalTel, String authCode, boolean isNewVersion){
		String msgId = "0102";
		String msgBody = "";
		if(isNewVersion) {
			msgBody = NormalTools.int2Hex(authCode.length()/2, 1)	// 鉴权码长度[2019]
					+ authCode										// 鉴权码内容
					+ NormalTools.fillZero("", 15)					// 终端IMEI[2019]
					+ NormalTools.fillZero("", 20);					// 软件版本号[2019]			
		}else {
			msgBody = authCode;
		}
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0200位置信息】兼容2011与2019
	 * 2019变化体现在报警标志/车辆状态/附加信息项的新增上(见Jt808类上方注释)，此处代码无需修改！
	  * 经纬度写死，高度/速度/方向随机，时间实时获取
	  * 位置附加信息根据报警标识自动添加
	 */
	public static String get0x0200Data(String teminalTel, Integer warnSign, Integer status) {
		return ProtocolsOf808.get0x0200Data(teminalTel, warnSign, status, true);
	}
	public static String get0x0200Data(String teminalTel, Integer warnSign, Integer status, boolean isNewVersion) {
		String msgId = "0200";
		String msgBody = ProtocolsOf808.get0x0200BodyData(warnSign, status);
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	/**
	  * 【0x0200位置信息】除时间自动生成外，其余内容均可指定
	 */
	public static String get0x0200Data(String teminalTel, Integer warnSign, Integer status, String lngLat, Integer hight, Integer speed, Integer direct, String locAppendInfo, boolean isNewVersion) {
		String msgId = "0200";
		String msgBody = get0x0200BodyData(warnSign, status, lngLat, hight, speed, direct, locAppendInfo);
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	public static String get0x0200BodyData(Integer warnSign, Integer status) {
		String msgBody = get0x0200BodyData(warnSign, status,			// 报警标志 + 车辆状态
							"116.666666,41.888888", 					// 经纬度（解析时先纬度后经度）
							NormalTools.getRandomInt(100, 1000), 		// 随机高度：100~1000米
							NormalTools.getRandomInt(800, 900), 		// 随机速度：80~90km/h
							NormalTools.getRandomInt(0, 10), 			// 随机方向：基本一路向北
							ProtocolsOf808LocAppend.getAllLocAppendInfo(warnSign));	// 与报警标识匹配的附件信息
		return msgBody;
	}
	public static String get0x0200BodyData(Integer warnSign, Integer status, String lngLat, Integer hight, Integer speed, Integer direct, String locAppendInfo) {
		String msgBody = NormalTools.int2Hex(warnSign, 4)					//报警标志(4)
						+ NormalTools.int2Hex(status, 4)					//状态(4)
						+ NormalTools.handleLngLat(lngLat.split(",")[1]) 	//纬度(4)
						+ NormalTools.handleLngLat(lngLat.split(",")[0])	//经度(4)
						+ NormalTools.int2Hex(hight)		// 速度(2)
						+ NormalTools.int2Hex(speed)		// 高度(2)
						+ NormalTools.int2Hex(direct)		// 方向(2)
						+ NormalTools.getTime()					// 日期时间：BCD[6]
						+ locAppendInfo;						// 位置附加信息
		return msgBody;
	}

	/** -------------------- 808 其他协议 -------------------- */
	
	
	/**
	 * 【0x0001 终端通用应答】2019版消息体内容无改动
	 */
	public static String get0x0001Data(String teminalTel) {
		return get0x0001Data(teminalTel, true);
	}
	public static String get0x0001Data(String teminalTel, boolean isNewVersion) {
		String msgId = "0001";
		String msgBody = NormalTools.int2Hex(666888)		// 对应平台消息的流水号
						+ "8103"							// 对应平台消息的msgId
						+ "00";								// 结果：0-成功/确认；1-失败；2-消息有误； 3-不支持；
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0003终端注销】兼容2011和2019
	 */
	public static String get0x0003Data(String teminalTel) {
		return get0x0003Data(teminalTel, true);
	}
	public static String get0x0003Data(String teminalTel, boolean isNewVersion) {
		String msgId = "0003";
		return getEmptyBodyData(msgId, teminalTel, isNewVersion);
	}
	
	/**
	 * 【0x0004查询服务器时间请求】2019新增协议
	 */
	public static String get0x0004Data(String teminalTel) throws Exception{
		return get0x0004Data(teminalTel, true);
	}
	public static String get0x0004Data(String teminalTel, boolean isNewVersion) throws Exception{
		if(!isNewVersion)
			throw new Exception(MsgTools.NOT_EXIST_ON_2011);
		return getEmptyBodyData("0004", teminalTel, isNewVersion);
	}
	
	/**
	 * 【0x0005终端补传分包请求】2019新增协议
	 * @param teminalTel
	 * @return
	 * @throws Exception
	 */
	public static String get0x0005Data(String teminalTel) throws Exception{
		return get0x0005Data(teminalTel, true);
	}
	public static String get0x0005Data(String teminalTel, boolean isNewVersion) throws Exception{
		if(!isNewVersion)
			throw new Exception(MsgTools.NOT_EXIST_ON_2011);
		String msgId = "0005";
		String msgBody = NormalTools.int2Hex(222222)
						+ NormalTools.int2Hex(3)
						+ NormalTools.int2Hex(666)
						+ NormalTools.int2Hex(777)
						+ NormalTools.int2Hex(888);
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0104 终端参数查询应答】2019版消息体内容无改动
	 */
	public static String get0x0104Data(String teminalTel) {
		return get0x0104Data(teminalTel, true);
	}
	public static String get0x0104Data(String teminalTel, boolean isNewVersion){
		String msgId = "0104";
		String msgBody = NormalTools.int2Hex(0)	// 应答流水号
				+ "01"						// 应答参数个数
				+ "00000001" 				// 参数ID（0x0001心跳间隔）
				+ "04" 						// 参数长度
				+ "0000001e";				// 参数值：16+14=30s
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}

	/**
	 * 【0x0107 终端属性查询应答】兼容2011和2019
	 * 2019版
	  *  终端类型：bit8之前无意义，现在表示“是否适用挂车”
	  *  终端型号：长度20->30
         *  终端ID：长度7->30
	 */
	public static String get0x0107Data(String teminalTel) {
		return get0x0107Data(teminalTel, true);
	}
	public static String get0x0107Data(String teminalTel, boolean isNewVersion){
		String msgId = "0107";
		String msgBody = "";
		if(isNewVersion) {
			msgBody = NormalTools.int2Hex(0)					// 终端类型 (2)[2019将原预留的bit8表示是否适用挂车]
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB005", 5))		// 制造商ID (5)
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB006", 30))		// 终端型号 (20)
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB007", 30))		// 终端ID (7)
					+ "00112233445566778899"				// 终端SIM卡ICCID (BCD[10])
					+ NormalTools.int2Hex(5, 1)				// 终端硬件版本号长度 (1)
					+ NormalTools.str2Hex("HELLO")			// 终端硬件版本号 (STRING)
					+ NormalTools.int2Hex(5, 1)				// 终端固件版本号长度 (1)
					+ NormalTools.str2Hex("WORLD")			// 终端固件版本号 (STRING)
					+ NormalTools.int2Hex(0)			// GNSS模块属性 (1)
					+ NormalTools.int2Hex(0);			// 通讯模块属性 (1)			
		}else {
			msgBody = NormalTools.int2Hex(0)					// 终端类型 (2)
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB005", 5))	// 制造商ID (5)
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB006", 20))	// 终端型号 (20)
					+ NormalTools.str2Hex(NormalTools.fillZeroAtTail("NB007", 7))	// 终端ID (7)
					+ "00112233445566778899"						// 终端SIM卡ICCID (BCD[10])
					+ NormalTools.int2Hex(5, 1)				// 终端硬件版本号长度 (1)
					+ NormalTools.str2Hex("HELLO")			// 终端硬件版本号 (STRING)
					+ NormalTools.int2Hex(5, 1)				// 终端固件版本号长度 (1)
					+ NormalTools.str2Hex("WORLD")			// 终端固件版本号 (STRING)
					+ NormalTools.int2Hex(0)			// GNSS模块属性 (1)
					+ NormalTools.int2Hex(0);			// 通讯模块属性 (1)						
		}
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0201 位置信息查询应答】2019版消息体内容无改动
	 */
	public static String get0x0201Data(String teminalTel) {
		return get0x0201Data(teminalTel, true);
	}
	public static String get0x0201Data(String teminalTel, boolean isNewVersion){
		String msgId = "0201";
		String msgBody = NormalTools.int2Hex(0)		// 应答流水号 (2)
				+ get0x0200BodyData(0, 7);			//位置汇报消息体
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0500 车辆控制应答】2019版修改了0x8500的数据格式，车辆控制应答格式无变化
	 */
	public static String get0x0500Data(String teminalTel) {
		return get0x0500Data(teminalTel, true);
	}
	public static String get0x0500Data(String teminalTel, boolean isNewVersion){
		String msgId = "0500";
		String msgBody = NormalTools.int2Hex(0)		// 应答流水号 (2)
				+ get0x0200BodyData(0, 7);			// 位置汇报消息体（状态位判断是否设置成功）
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
		
	/**
	 * 【0x0608 查询区域或路线数据应答】2019新增协议
	 */
	public static String get0x0608Data(String teminalTel, int areaType, int areaCount) throws Exception{
		return get0x0608Data(teminalTel, areaType, areaCount, true);
	}
	public static String get0x0608Data(String teminalTel, int areaType, int areaCount, boolean isNewVersion) throws Exception{
		if(!isNewVersion)
			throw new Exception(MsgTools.NOT_EXIST_ON_2011);
		String msgId = "0608";
		
		String msgBody = NormalTools.int2Hex(areaType, 1)			// 查询类型
						+ NormalTools.int2Hex(areaCount, 4);		// 数据数量
		
		for(int i = 0; i < areaCount; i ++)
			msgBody += ProtocolsOf808.getAreaDataOfType(areaType);
		
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/** 【获取各区域类型数据】 */
	public static String getAreaDataOfType(int areaType) throws Exception{
		switch(areaType) {
		case 1:
			return getAreaDataOfType1or2(1);		// 1表示圆形区域
		case 2:
			return getAreaDataOfType1or2(2);		// 2表示矩形区域
		case 3:
			return getAreaDataOfType3();			// 3表示多边形区域：可指定顶点数
		case 4:
			return getAreaDataOfType4();			// 4表示路线：可指定线路拐点数
		default:
			throw new Exception("区域类型只能是1~4的数值！！！");
		}
	}

	/** 获取圆形或矩形区域的数据（areaType=1或2） */
	public static String getAreaDataOfType1or2(int areaType) {
		int areaProperty = 7;
		String name = NormalTools.str2Hex("中华南大街" + random.nextInt() + "号");
		String areaData = NormalTools.int2Hex(areaType * 10000 + random.nextInt(100), 4)		// 区域ID
				+ NormalTools.int2Hex(areaProperty)												// 区域属性
				+ NormalTools.int2Hex(41666000 + random.nextInt(1000), 4)			// 圆形中心纬度或矩形左上纬度
				+ NormalTools.int2Hex(116777000 + random.nextInt(1000), 4)			// 圆形中心经度或矩形左上经度
				
				// [区域类型为1时此处为圆形区域半径，为2时表示矩形区域右下角坐标]
				+ (areaType == 1 ? 
				  NormalTools.int2Hex(random.nextInt(1000000), 4)					// 圆形半径
				:(NormalTools.int2Hex(41222000 + random.nextInt(1000), 4)			// 矩形右下点纬度
				+ NormalTools.int2Hex(116888000 + random.nextInt(1000), 4)))		// 矩形右下点经度
				
				// [区域属性第0位为0时则没有起止时间字段：起始时间、结束时间]
				+ ((areaProperty & 1) == 0 ?"": ProtocolsOf808.getStartEndTime(-10, 0))
				
				// [区域属性第1位为0时则没有超速相关字段：最高速度、超速持续时间、夜间最高速度]
				+ ((areaProperty & 2) == 0 ?"": ProtocolsOf808.getOverSpeedData(1250, 30, 1100))
				
				+ NormalTools.int2Hex(name.length()/2)	// 区域名称长度
				+ name;									// 区域名称
		return areaData;
	}
	
	/** 获取多边形区域的数据 */
	public static String getAreaDataOfType3() {
		return getAreaDataOfType3(5);
	}
	public static String getAreaDataOfType3(int peakCount) {
		int areaProperty = 8;
		String peaksData = "";
		for(int i = 0; i < peakCount; i ++) {
			peaksData += NormalTools.int2Hex(41666000 + random.nextInt(1000), 4);	// 顶点纬度
			peaksData += NormalTools.int2Hex(116777000 + random.nextInt(1000), 4);	// 顶点经度
		}
		String name = NormalTools.str2Hex("中华南大街" + random.nextInt() + "号");
		String areaData = NormalTools.int2Hex(30000 + random.nextInt(100), 4)		// 区域ID
				+ NormalTools.int2Hex(areaProperty)											// 区域属性

				// [区域属性第0位为0时则没有起止时间字段：起始时间、结束时间]
				+ ((areaProperty & 1) == 0 ?"": ProtocolsOf808.getStartEndTime(-10, 0))
				
				// [区域属性第1位为0时则没有超速相关字段：最高速度、超速持续时间、夜间最高速度]
				+ ((areaProperty & 2) == 0 ?"" :
				(NormalTools.int2Hex(1250) + NormalTools.int2Hex(30, 1)))
				
				// 顶点项信息
				+ NormalTools.int2Hex(peakCount)		// 区域顶点数
				+ peaksData								// 区域顶点项列表
				
				// [区域属性第1位为0时则没有超速相关字段：夜间最高速度]
				+ ((areaProperty & 2) == 0 ?"" :NormalTools.int2Hex(1100))
				
				+ NormalTools.int2Hex(name.length()/2)	// 区域名称长度
				+ name;									// 区域名称
		return areaData;
	}
	
	/** 获取路线区域的数据 */
	public static String getAreaDataOfType4() {
		return getAreaDataOfType4(5);
	}
	public static String getAreaDataOfType4(int lineNodeCount) {
		int areaProperty = 7;
		String lineNodesData = "";
		for(int i = 0; i < lineNodeCount; i ++)
			lineNodesData += getLineNodeData();
		String name = NormalTools.str2Hex("中华南大街" + random.nextInt() + "号");
		String areaData = NormalTools.int2Hex(40000 + random.nextInt(100), 4)		// 区域ID
				+ NormalTools.int2Hex(areaProperty)											// 区域属性

				// [区域属性第0位为0时则没有起止时间字段：起始时间、结束时间]
				+ ((areaProperty & 1) == 0 ?"": ProtocolsOf808.getStartEndTime(-10, 0))
				
				+ NormalTools.int2Hex(lineNodeCount)	// 拐点总数
				+ lineNodesData							// 拐点项列表
				+ NormalTools.int2Hex(name.length()/2)	// 路线名称长度
				+ name;									// 路线名称
		return areaData;
	}
	public static String getLineNodeData() {
		int roadProperty = random.nextInt(4);
		String lineNodeData = NormalTools.int2Hex(800000 + random.nextInt(1000), 4)		// 拐点ID
					+ NormalTools.int2Hex(888888, 4)							// 路段ID
					+ NormalTools.int2Hex(41222000 + random.nextInt(1000), 4)		// 拐点纬度
					+ NormalTools.int2Hex(116888000 + random.nextInt(1000), 4)		// 拐点经度
					+ NormalTools.int2Hex(random.nextInt(100), 1)					// 路段宽度
					+ NormalTools.int2Hex(roadProperty, 1);				// 路段属性
		// 路段属性的bit0为1时才有此内容
		if((roadProperty & 1) > 0) {
			lineNodeData += NormalTools.getRandomHex(600, 900)			// 路段行驶时间过长阈值
					+ NormalTools.getRandomHex(300, 480);				// 路段行驶时间不足阈值
		}
		// 路段属性的bit1为1时才有此内容
		if((roadProperty & 2) > 0) {
			lineNodeData += NormalTools.getRandomHex(100, 120)			// 路段最高速度
					+ NormalTools.int2Hex(random.nextInt(200), 1)		// 路段超速持续时间
					+ NormalTools.getRandomHex(80, 100);				// 路段夜间最高速度
		}
		return lineNodeData;			
	}
	
	// 获取0x0608中的起止时间
	private static String getStartEndTime(int start, int end) {
		return NormalTools.getTime(start) + NormalTools.getTime(end);
	}
	// 获取0x0608中的超速信息
	private static String getOverSpeedData(int maxSpeed, int overSpeedTime, int nightMaxSpeed) {
		return NormalTools.int2Hex(maxSpeed) + NormalTools.int2Hex(overSpeedTime, 1) + NormalTools.int2Hex(nightMaxSpeed);
	}
	
	/**
	 * 【0x0700 行驶记录数据上传】2019版增加了对数据块字段的定义，相比[2011基础版]协议数据无变化
	 *  采集行车记录仪相关信息，共计15个命令字（对应不同的数据块格式）
	 */
	public static String get0x0700Data(String teminalTel) {
		return get0x0700Data(teminalTel, true);
	}
	public static String get0x0700Data(String teminalTel, boolean isNewVersion) {
		String msgId = "0700";
		String msgBody = NormalTools.int2Hex(666)	// 0x8700消息头中的流水号
						+ "00"						// 命令字：0x00为采集记录仪执行标准的版本（GB/T 19056：表A.5）
						+ "12"						// 执行年份标准后两位（BCD码）
						+ "00";						// 修改单号
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0701 电子运单上报】相比[2011基础版]，2019升级版消息体内容无改动
	 */
	public static String get0x0701Data(String teminalTel) {
		return get0x0701Data(teminalTel, true);
	}
	public static String get0x0701Data(String teminalTel, boolean isNewVersion) {
		String dataPack = NormalTools.str2Hex("电子运单数据包，应该是字符串类型的吧!");

		String msgId = "0701";
		String msgBody = NormalTools.int2Hex(dataPack.length()/2, 4)	// 电子运单长度
						+ dataPack;										// 电子运单数据包
		
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0702 驾驶员身份信息采集上报】兼容2011和2019
	 *  2019版在消息体最后添加了一个“驾驶员身份证号”
	 */
	public static String get0x0702Data(String teminalTel) {
		return get0x0702Data(teminalTel, true);
	}
	public static String get0x0702Data(String teminalTel, boolean isNewVersion) {
		String driverName = NormalTools.str2Hex("张三丰");
		String orgName = NormalTools.str2Hex("银河系政府办事处");
		
		String msgId = "0702";
		String msgBody = "01"						// 状态：01卡插入 02卡拔出
						+ NormalTools.getTime()		// 卡插拔时间：BCD[6]
						+ "00"						// IC卡读取结果：00-读取成功；01~04不同类型的读取失败
						+ NormalTools.int2Hex(driverName.length()/2, 1)		// 驾驶员姓名长度
						+ driverName										// 驾驶员姓名
						+ NormalTools.fillZero(NormalTools.str2Hex("hello"), 20)		// 从业资格证编码
						+ NormalTools.int2Hex(orgName.length()/2, 1)		// 发证机构名称长度
						+ orgName											// 发证机构名称
						+ "20501201";										// 证件有效期：BCD[4] YYYYMMDD
		if(isNewVersion)
			msgBody += NormalTools.fillZero(NormalTools.str2Hex("130135197512252337"), 20);	// 驾驶员身份证号[2019新增字段！！！]
		
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0704 定位数据批量上传】2019新增，但[2011升级版]已完整实现
	 */
	public static String get0x0704Data(String teminalTel) {
		return get0x0704Data(teminalTel, true);
	}
	public static String get0x0704Data(String teminalTel, boolean isNewVersion){
		String msgId = "0704";
		String msg1 = get0x0200BodyData(0, 7);
		String msg2 = get0x0200BodyData(0, 15);
		
		String msgBody = NormalTools.int2Hex(2)		// 数据项个数 (2)
					+ NormalTools.int2Hex(1, 1)		// 位置数据类型 (0-正常批量汇报，1-盲区补报)
					+ NormalTools.int2Hex(msg1.length() / 2)	// 数据体1长度 (2)
					+ msg1										// 数据体1 (不定)
					+ NormalTools.int2Hex(msg2.length() / 2)	// 数据体2长度 (2)
					+ msg2;										// 数据体2 (不定)
		
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0705 CAN总线数据】2019新增，但[2011升级版]已完整实现
	 * CanId 结构说明
		 *  bit31通道号:0-CAN1,1-CAN2  
		 *  bit30帧类型:0-标准帧,1-扩展帧  
		 *  bit29数据采集方式:0-原始,1-区间平均值  
		 *  bit28-bit0总线ID
	 */
	public static String get0x0705Data(String teminalTel) {
		return get0x0705Data(teminalTel, true);
	}
	public static String get0x0705Data(String teminalTel, boolean isNewVersion){
		String msgId = "0705";
		String msgBody = NormalTools.int2Hex(2)							// 数据项个数 (2)
				+ NormalTools.getCanTime()								// CAN总线数据接收时间 (5)
				+ NormalTools.int2Hex(random.nextInt(100000000), 4)				// CAN ID1 (4)
				+ NormalTools.getRandomHexStr(8)								// CAN DATA1 (8)
				+ NormalTools.int2Hex(random.nextInt(100000000) * (-1), 4)		// CAN ID2 (4)
				+ NormalTools.getRandomHexStr(8);								// CAN DATA2 (8)
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0800 多媒体事件数据上传】2019版消息体内容无改动
	 */
	public static String get0x0800Data(String teminalTel) {
		return get0x0800Data(teminalTel, true);
	}
	public static String get0x0800Data(String teminalTel, boolean isNewVersion) {
		String msgId = "0800";
		String msgBody = NormalTools.int2Hex(1689999, 4)	// 多媒体ID
				+ "00"					// 多媒体类型：0-图像；1-音频；2-视频
				+ "00"					// 多媒体格式编码：0-JPEG；1-TIF；2-MP3；3-WAV；4-WMV；
				+ "02"					// 事件项编码：0-平台下发；1-定时动作；2-抢劫报警；3-碰撞侧翻预警；4-开车门；5-关车门；
										// 			  6-车速从小于20km/h超过20km/h；7-定距拍照；
				+ "03";					// 通道ID
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0801多媒体数据上传】2019版新增了位置信息汇报体字段，[2011升级版]已包含该字段
	 *  相对于[2011升级版]来说，事件项编码中增加了：4-打开车门；5-关闭车门两项
	 */
	public static List<String> get0x0801Data(String teminalTel) {
		return get0x0801Data(teminalTel, true);
	}
	public static List<String> get0x0801Data(String teminalTel, boolean isNewVersion) {
		String mediaData = NormalTools.getHexStrFromFile("java.jpg");
		String msgBody = NormalTools.int2Hex(1689999, 4)	// 多媒体ID
						+ "00"					// 多媒体类型：0-图像；1-音频；2-视频
						+ "00"					// 多媒体格式编码：0-JPEG；1-TIF；2-MP3；3-WAV；4-WMV；
						+ "02"					// 事件项编码：0-平台下发；1-定时动作；2-抢劫报警；3-碰撞侧翻预警；4-开车门；5-关车门；
						+ "03"					// 通道ID
						+ ProtocolsOf808.get0x0200BodyData(0, 0, "116.33333,41.999999", 20, 700, 0, "")		// 位置信息会报消息体
						+ mediaData;					// 多媒体数据包
		
		int size = 1024;				// 字符串长度1024，对应字节数为512
		int len = msgBody.length();		// 字符串总长度（字节数*2）
		int total = (len / size) + (len % size == 0? 0: 1);
		
		List<String> datas = new ArrayList<String>();
		for(int i = 1; i <= total; i ++) {
			String slice = msgBody.substring((i-1)*size, Integer.min(i*size, len));
			datas.add(get0x0801SingleMsg(teminalTel, slice, total, i));
		}		
		return datas;
	}
	
	public static String get0x0801SingleMsg(String teminalTel, String msgBody, int total, int sn) {
		String msgHeader = "0801" 														// 消息ID（2）
				+ NormalTools.int2Hex( (1 << 14) + (1 << 13) + msgBody.length()/2 ) 	// 消息体属性（2）
				+ NormalTools.int2Hex(1, 1)												// 协议版本号（1）
				+ NormalTools.fillZero(teminalTel, 10) 									// 终端手机号（10）
				+ "0000"																// 消息流水号（2）
				+ NormalTools.int2Hex(total)		// 分包总数
				+ NormalTools.int2Hex(sn);			// 包序号
		// 拼接消息
		String rawMsg = msgHeader + msgBody + MsgTools.getCheckCode(msgHeader+msgBody);
		
		// 转义并返回
		return "7e" + MsgTools.encTM(rawMsg) + "7e";
	}
	
	
	
	
	
	
	/**
	 * 【0x0805 摄像头立即拍摄命令应答】2019新增，[2011升级版]已完整实现
	 */
	public static String get0x0805Data(String teminalTel) {
		return get0x0805Data(teminalTel, true);
	}
	public static String get0x0805Data(String teminalTel, boolean isNewVersion){
		String msgId = "0805";

		String msgBody = NormalTools.int2Hex(0)  		// 协议0x8801摄像头立即拍摄指令中的流水号
				  		+ NormalTools.int2Hex(0, 1)  	// 结果：0-成功；1-失败；2-通道不支持
				  		+ NormalTools.int2Hex(2)	  		// 多媒体ID个数
				  		+ NormalTools.int2Hex(40111111, 4)  // ID1
				  		+ NormalTools.int2Hex(6666, 4);  	// ID2

		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0900 数据上行透传】2019修改了透传消息类型描述，与[2011升级版]比较无变化
	 */
	public static String get0x0900Data(String teminalTel) {
		return get0x0900Data(teminalTel, true);
	}
	public static String get0x0900Data(String teminalTel, boolean isNewVersion) {
		String msgId = "0900";
		String msgBody = "41"											// 透传消息类型：约定有4种，可自定义（表83）；
						+ NormalTools.str2Hex("看尔乃插标卖首...");	// 透传消息内容
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【0x0901 数据压缩上报】 2019版消息体内容无改动
	 */
	public static String get0x0901Data(String teminalTel) {
		return get0x0901Data(teminalTel, true);
	}
	public static String get0x0901Data(String teminalTel, boolean isNewVersion) {		
		String msgId = "0901";
		String gzipMsg = MsgTools.gzip("天王盖地虎，宝塔镇河妖！");
		
		String msgBody = NormalTools.int2Hex(gzipMsg.length()/2, 4)		// 压缩消息长度
						+ gzipMsg;										// 压缩消息体
		
		return MsgTools.getMessage(msgId, teminalTel, msgBody, isNewVersion);
	}
	
	/**
	 * 【生成消息体为空的协议数据】
	 */
	public static String getEmptyBodyData(String msgId, String teminalTel, boolean isNewVersion) {
		return MsgTools.getMessage(msgId, teminalTel, "", isNewVersion);
	}
}
