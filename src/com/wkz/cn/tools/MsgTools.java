package com.wkz.cn.tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;


public class MsgTools {
	public static final String NOT_EXIST_ON_2011 = "JT808-2011版本中无此协议";
	
	public static Map<String, String> msgDict = getMsgDict();
	
	// 协议列表
	private static Map<String, String> getMsgDict(){
		Map<String, String> msgDict = new HashMap<>();
		// Jt808-苏标
		msgDict.put("0x8001", "Jt808平台通用应答");
		msgDict.put("0x0002", "Jt808终端心跳");
		msgDict.put("0x8107", "Jt808查询终端属性");
		msgDict.put("0x0100", "Jt808终端注册");
		msgDict.put("0x8100", "Jt808终端注册应答");
		msgDict.put("0x0102", "Jt808终端鉴权");
		msgDict.put("0x0200", "Jt808位置信息汇报");
		msgDict.put("0x0608", "Jt808查询区域或线路数据应答");
		// Jt809
		msgDict.put("0x1201", "Jt809车辆注册信息");
		msgDict.put("0x1202", "Jt809实时位置信息");
		msgDict.put("0x1001", "Jt809主链路登录请求");
		msgDict.put("0x1002", "Jt809主链路登录应答");
		msgDict.put("0x1005", "Jt809主链路连接保持请求");
		msgDict.put("0x1006", "Jt809主链路连接保持应答");
		msgDict.put("0x9001", "Jt809从链路连接请求");
		msgDict.put("0x9002", "Jt809从链路连接应答");
		msgDict.put("0x9005", "Jt809从链路连接保持请求");
		msgDict.put("0x9006", "Jt809从链路连接保持应答");
		msgDict.put("0x9301", "Jt809从链路平台查岗请求");
		msgDict.put("0x1301", "Jt809从链路平台查岗应答");
		msgDict.put("0x9302", "Jt809从链路下发平台间报文请求");
		msgDict.put("0x1302", "Jt809从链路下发平台间报文应答");
		msgDict.put("0x9401", "Jt809从链路报警督办请求");
		msgDict.put("0x1401", "Jt809从链路报警督办应答");
		
		return msgDict;
	}
	
	// 生成完整消息
	public static String getMessage(String msgId, String teminalTel, String msgBody, boolean isNewVersion){
		// 拼接消息头
		String msgHeader = "";
		if(isNewVersion) {
			// TODO 消息体过长超过10个bit(1023)时，会把bit11~bit15都占上，导致消息头混乱；
			// 需要进行分包处理！
			msgHeader = msgId 													// 消息ID（2）
					+ NormalTools.int2Hex( (1 << 14) + msgBody.length()/2 ) 	// 消息体属性（2）
					+ NormalTools.int2Hex(1, 1)									// 协议版本号（1）
					+ NormalTools.fillZero(teminalTel, 10) 						// 终端手机号（10）
					+ "0000";													// 消息流水号（2）			
		}else {
			msgHeader = msgId 													// 消息ID（2）
					+ NormalTools.int2Hex( msgBody.length()/2 ) 				// 消息体属性（2）
					+ NormalTools.fillZero(teminalTel, 6) 						// 终端手机号（6）
					+ "0000";													// 消息流水号（2）						
		}
		
		// 拼接消息
		String rawMsg = msgHeader + msgBody + getCheckCode(msgHeader+msgBody);
		
		// 转义并返回
		return "7e" + encTM(rawMsg) + "7e";
	}
	
	// 获取校验码
	public static String getCheckCode(String str){
		int number = Integer.parseInt(str.substring(0, 2), 16);
		for (int i = 2; i < str.length(); i += 2){
			number ^= Integer.parseInt(str.substring(i, i+2), 16);
		}
		String checkCode = Integer.toHexString(number);
		if (number < 16)
			return "0" + checkCode;
		return checkCode;
	}
	
    // 【JT808转义】
    public static String encTM(String str){
    	String tmp = "";
    	String newStr = "";
    	for(int i = 0; i < str.length(); i += 2){
    		tmp = str.substring(i, i + 2);
    		if( tmp.equals("7d") ){
    			newStr += "7d01";
    		}else if( tmp.equals("7e") ){
    			newStr += "7d02";
    		}else {
    			newStr += tmp;
    		}
    	}
    	return newStr;
    }
    // 【JT808反转义】
    public static String decTM(String str){
    	String newStr = "";
    	String tmp = "";
    	for(int i = 2; i < str.length() - 4; i += 2){
    		tmp = str.substring(i, i + 2);
    		if(str.substring(i, i+4).equals("7d01")){
    			newStr += "7d";
    			i += 2;
    		}else if(str.substring(i, i+4).equals("7d02")){
    			newStr += "7e";
    			i += 2;
    		}else{
    			newStr += tmp;
    		}
    	}
    	return "7e" + newStr + str.substring(str.length()-4, str.length()-2) + "7e";
    }
    
	
	/** GZIP压缩：输入为字符串，输出为压缩后的十六进制字符串形式 */
	public static String gzip(String str){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream gzip;
		try {
			gzip = new GZIPOutputStream(bos);
			gzip.write(NormalTools.str2Bytes(str));
			gzip.finish();
			gzip.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] ret = bos.toByteArray();
		try {
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return NormalTools.bin2Hex(ret);
	}
	/** GZIP压缩：输入输出均为byte[] */
	public static byte[] gzip(byte[] data){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		GZIPOutputStream gzip;
		try {
			gzip = new GZIPOutputStream(bos);
			gzip.write(data);
			gzip.finish();
			gzip.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		byte[] ret = bos.toByteArray();
		try {
			bos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
    
    
    /********************************** JT809相关 *******************************************/

	/**
	 * 根据msgId和msgBody生成809完整消息
	 */
	public static String get809Message(String msgId, Integer accessCode, String msgBody) {
		String msgHeader = NormalTools.int2Hex(34 + msgBody.length()/2, 4)	// 消息总长度(4)
						 + "02020202"										// 报文序列号(4)
						 + msgId									// 业务数据类型(2)
						 + NormalTools.int2Hex(accessCode, 4)		// 平台唯一标识码(4)
						 + "000001"						// 协议版本号
						 + "00"							// 加密标识（0-不加密； 1-加密）
						 + "00000000"					// 加密秘钥
						 + NormalTools.get809UTCHex();	// UTC时间-2019版新加字段
		
		String msgContent = msgHeader + msgBody;
		
		String totalMsg = "5b"
				   + msgContent
				   + MsgTools.get809CRC(NormalTools.hex2Bin(msgContent))
				   + "5d";
		
		return NormalTools.bin2Hex(MsgTools.encTM809(NormalTools.hex2Bin(totalMsg)));
	}
	
	/**
	  * 计算校验码
	  * CRC-16/CCITT-FALSE x16+x12+x5+1 算法
	  * Name:CRC-16/CCITT-FAI
	  */
	public static String get809CRC(byte[] bs) {
		int crc = 0xFFFF;
		int polynomial = 0x1021;
		for (byte b : bs) {
			for (int i = 0; i < 8; i++) {
				boolean bit = ((b >> (7 - i) & 1) == 1);
				boolean c15 = ((crc >> 15 & 1) == 1);
				crc <<= 1;
				if (c15 ^ bit) 
					crc ^= polynomial;
			}
		}
		crc &= 0xffff;
		return NormalTools.int2Hex(crc);
	 }
	
	/**
	 * 接收消息转义
	 * 0x5a 0x01 ==> 0x5b
	 * 0x5a 0x02 ==> 0x5a
	 * 0x5e 0x01 ==> 0x5d
	 * 0x5e 0x02 ==> 0x5e
	 * @param msg 包括头标识、数据头、数据体、校验码、尾标识
	 * @return 返回转义后发送的完整信息，包括 头标识、数据头、数据体、校验码、尾标识
	 */
	public static String decTM809(String msg) {
		return NormalTools.bin2Hex(decTM809(NormalTools.hex2Bin(msg)));
	}
	public static byte[] decTM809(byte[] msg) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(msg.length - 1);
		for (int i = 1; i < msg.length - 1; i++) {
			if (msg[i] == 0x5a && msg[i + 1] == 0x01) {
				byteBuffer.put((byte) 0x5b);
				i++;
			} else if (msg[i] == 0x5a && msg[i + 1] == 0x02) {
				byteBuffer.put((byte) 0x5a);
				i++;
			} else if (msg[i] == 0x5e && msg[i + 1] == 0x01) {
				byteBuffer.put((byte) 0x5d);
				i++;
			} else if (msg[i] == 0x5e && msg[i + 1] == 0x02) {
				byteBuffer.put((byte) 0x5e);
				i++;
			} else {
				byteBuffer.put(msg[i]);
			}
		}
		int length = byteBuffer.position();
		byte[] retByte = new byte[length];
		byteBuffer.position(0);
		byteBuffer.get(retByte, 0, length);
		
		//return retByte;
		int len = retByte.length;
		byte[] finalByte = new byte[len + 2];
		finalByte[0] = 0x5b;
		finalByte[len - 2] = 0x5d;
		System.arraycopy(retByte, 0, finalByte, 1, len);
		return finalByte;
	}

	/**
	 * 发送消息转义（头标识和尾标识不转义）
	 * 0x5b ==> 0x5a 0x01
	 * 0x5a ==> 0x5a 0x02
	 * 0x5d ==> 0x5e 0x01
	 * 0x5e ==> 0x5e 0x02
	 * @param msg 包括头标识、数据头、数据体、校验码、尾标识
	 * @return 返回转义后发送的完整信息，包括 头标识、数据头、数据体、校验码、尾标识
	 */
	public static String encTM809(String msg) {
		return NormalTools.bin2Hex(encTM809(NormalTools.hex2Bin(msg)));
	}
	public static byte[] encTM809(byte[] msg) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(msg.length * 2);
		byteBuffer.put((byte) 0x5b);
		for (int i = 1; i < msg.length - 1; i++) {
			if (msg[i] == 0x5b) {
				byteBuffer.put((byte) 0x5a);
				byteBuffer.put((byte) 0x01);
			} else if (msg[i] == 0x5a) {
				byteBuffer.put((byte) 0x5a);
				byteBuffer.put((byte) 0x02);
			} else if (msg[i] == 0x5d) {
				byteBuffer.put((byte) 0x5e);
				byteBuffer.put((byte) 0x01);
			} else if (msg[i] == 0x5e) {
				byteBuffer.put((byte) 0x5e);
				byteBuffer.put((byte) 0x02);
			} else {
				byteBuffer.put(msg[i]);
			}
		}
		byteBuffer.put((byte) 0x5d);
		int length = byteBuffer.position();
		byte[] retByte = new byte[length];
		byteBuffer.position(0);
		byteBuffer.get(retByte, 0, length);
		return retByte;
	}
	


	// 809数据加解密(混淆)--两次混淆则数据还原 
    public static String encrypt809(Long m1, Long a1, Long c1, Long key, String data) {		//TODO 2019版此处的加密算法需调整？！！
    	return NormalTools.bin2Hex(encrypt809(m1, a1, c1, key, NormalTools.hex2Bin(data)));
    }
    private static byte[] encrypt809(Long m1, Long a1, Long c1, Long key, byte[] data) {
        key = key == 0 ? 1: key;
        Long mkey = m1 == 0 ? 1: m1;
        
        int idx = 0;
        while (idx < data.length) {
            key = a1 * (key % mkey) + c1;
            key = key % (0xffffffffL + 1);
            data[idx] ^= ((key >> 20) & 0xFF);
            idx++;
        }
        return data;
    }

	// 获取809协议上级平台信息
	public static SuperiorInfo getSuperior(Integer accessCode) {
		SuperiorInfo superior = new SuperiorInfo();
		
		
		
		return superior;
	}

}
