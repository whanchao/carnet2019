package com.wkz.cn.tools;

public class SuperiorInfo {
	private Integer id;
	private String superior_name;
	private String superior_user;
	private String superior_pwd;
	private String superior_host;
	private Integer superior_port;
	private Integer msg_code;
	private Long encrypt_m1;
	private Long encrypt_a1;
	private Long encrypt_c1;
	private Long encrypt_key;
	private Integer encrypt_flag;
	private Integer is_deleted;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSuperior_name() {
		return superior_name;
	}
	public void setSuperior_name(String superior_name) {
		this.superior_name = superior_name;
	}
	public String getSuperior_user() {
		return superior_user;
	}
	public void setSuperior_user(String superior_user) {
		this.superior_user = superior_user;
	}
	public String getSuperior_pwd() {
		return superior_pwd;
	}
	public void setSuperior_pwd(String superior_pwd) {
		this.superior_pwd = superior_pwd;
	}
	public String getSuperior_host() {
		return superior_host;
	}
	public void setSuperior_host(String superior_host) {
		this.superior_host = superior_host;
	}
	public Integer getSuperior_port() {
		return superior_port;
	}
	public void setSuperior_port(Integer superior_port) {
		this.superior_port = superior_port;
	}
	public Integer getMsg_code() {
		return msg_code;
	}
	public void setMsg_code(Integer msg_code) {
		this.msg_code = msg_code;
	}
	public Long getEncrypt_m1() {
		return encrypt_m1;
	}
	public void setEncrypt_m1(Long encrypt_m1) {
		this.encrypt_m1 = encrypt_m1;
	}
	public Long getEncrypt_a1() {
		return encrypt_a1;
	}
	public void setEncrypt_a1(Long encrypt_a1) {
		this.encrypt_a1 = encrypt_a1;
	}
	public Long getEncrypt_c1() {
		return encrypt_c1;
	}
	public void setEncrypt_c1(Long encrypt_c1) {
		this.encrypt_c1 = encrypt_c1;
	}
	public Long getEncrypt_key() {
		return encrypt_key;
	}
	public void setEncrypt_key(Long encrypt_key) {
		this.encrypt_key = encrypt_key;
	}
	public Integer getEncrypt_flag() {
		return encrypt_flag;
	}
	public void setEncrypt_flag(Integer encrypt_flag) {
		this.encrypt_flag = encrypt_flag;
	}
	public Integer getIs_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(Integer is_deleted) {
		this.is_deleted = is_deleted;
	}
	
	public SuperiorInfo() {
	}
	public SuperiorInfo(Integer id, String superior_name, String superior_user, String superior_pwd,
			String superior_host, Integer superior_port, Integer msg_code, Long encrypt_m1, Long encrypt_a1,
			Long encrypt_c1, Long encrypt_key, Integer encrypt_flag, Integer is_deleted) {
		super();
		this.id = id;
		this.superior_name = superior_name;
		this.superior_user = superior_user;
		this.superior_pwd = superior_pwd;
		this.superior_host = superior_host;
		this.superior_port = superior_port;
		this.msg_code = msg_code;
		this.encrypt_m1 = encrypt_m1;
		this.encrypt_a1 = encrypt_a1;
		this.encrypt_c1 = encrypt_c1;
		this.encrypt_key = encrypt_key;
		this.encrypt_flag = encrypt_flag;
		this.is_deleted = is_deleted;
	}
	
	@Override
	public String toString() {
		return "SuperiorInfo [id=" + id + ", superior_name=" + superior_name + ", superior_user=" + superior_user
				+ ", superior_pwd=" + superior_pwd + ", superior_host=" + superior_host + ", superior_port="
				+ superior_port + ", msg_code=" + msg_code + ", encrypt_m1=" + encrypt_m1 + ", encrypt_a1=" + encrypt_a1
				+ ", encrypt_c1=" + encrypt_c1 + ", encrypt_key=" + encrypt_key + ", encrypt_flag=" + encrypt_flag
				+ ", is_deleted=" + is_deleted + "]";
	}
}
