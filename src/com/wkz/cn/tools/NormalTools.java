package com.wkz.cn.tools;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;


public class NormalTools {
	public static Random random = new Random();
	private static SimpleDateFormat sdf = new SimpleDateFormat("YYMMddHHmmss");	
	private static SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static ObjectMapper objMapper = new ObjectMapper();		

	
	public static void main(String[] args) {
		System.out.println(NormalTools.hexStr2Str(NormalTools.getStrWithOutSpace("B2 E9 B8 DA B2 E9 B8 DA A3 AC CC EC CD F5 B8 C7 B5 D8 BB A2 2E 2E 2E 20 2E 2E 2E")));
	}

	// 字节数字转整型
	public static int bytes2Int(byte[] bytes, int start, int end) {
		if(end - start > 4)
			System.out.println("超过了4个字节，存在溢出！！！");
		int res = 0;
		for(int i = start; i < end; i ++)
			res = res * 256 + (bytes[i] & 0xff);
		return res;
	}

	/** -------------- 类型转换 -------------- */
	
	// 整型转十六进制（String形式）
	public static String int2Hex(int num){
		return int2Hex(num, 2);
	}
	public static String int2Hex(int num, int bytesLen){
		String hex = Integer.toHexString(num);
		int hex_len = hex.length();
		if(num < 0)
			return hex.substring(hex_len - bytesLen * 2, hex_len);
		else
			return NormalTools.fillZero(hex, bytesLen);
	}
	// Long类型转十六进制（String形式）
	public static String long2Hex(long num) {
		return long2Hex(num, 8);
	}
	public static String long2Hex(long num, int bytesLen) {
		String hex = Long.toHexString(num);
		int hex_len = hex.length();
		for(int i = hex_len; i < bytesLen * 2; i ++)
			hex = "0" + hex;
		return hex;
	}
	//十六进制（String形式）转整型
	public static int hexStr2Int(String hexStr){
		return Integer.parseInt(hexStr, 16);
	}
	
	// 整型转字节数组
	public static byte[] int2Bytes(int number, int bytesLen){
		ByteBuffer buffer = ByteBuffer.allocate(bytesLen);
		buffer.putInt(number);
		return buffer.array();
	}
	
	// 字符串转字节数组
	public static byte[] str2Bytes(String str) {
		byte[] bytes = null;
		try {
			bytes = str.getBytes("GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return bytes;
	}
	public static byte[] str2Bytes(String str, int bytesLen){
		byte[] finalBytes = new byte[bytesLen];
		byte[] strBytes = null;
		try {
			strBytes = str.getBytes("GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		System.arraycopy(strBytes, 0, finalBytes, bytesLen - strBytes.length, strBytes.length);
		return finalBytes;
	}
	
	// 字符串转十六进制（String形式）
	public static String str2Hex(String str) {
	    byte[] strBytes = null;
		try {
			strBytes = str.getBytes("GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	    return NormalTools.bin2Hex(strBytes);
	}
	public static String str2Hex(String str, int bytesLen) {
		return fillZero(str2Hex(str), bytesLen);
	}
	
	// 十六进制（String形式）转字符串
	public static String hexStr2Str(String hexStr){
		hexStr = NormalTools.getStrWithOutSpace(hexStr.toLowerCase());
		String str = "0123456789abcdef";
		char[] hexs = hexStr.toCharArray();
		byte[] bytes = new byte[hexStr.length() / 2];
		int n;
		for (int i = 0; i < bytes.length; i++) {
			n = str.indexOf(hexs[2 * i]) * 16;
			n += str.indexOf(hexs[2 * i + 1]);
			bytes[i] = (byte) (n & 0xff);
		}
		
		String finalStr = null;
		try {
			finalStr = new String(bytes, "GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return finalStr;
	}
	
	// 十六进制字符串 --> 字节数组
	public static byte[] hex2Bin(String hex) {
		hex = hex.toLowerCase();
	    int len = (hex.length() / 2);  
	    byte[] result = new byte[len];  
	    char[] achar = hex.toCharArray(); 
	    for (int i = 0; i < len; i++) {  
	    	int pos = i * 2;  
	    	result[i] = (byte) (toByte(achar[pos]) << 4 | toByte(achar[pos + 1]));  
	    }  
	    return result;  
	}
	public static byte toByte(char c) {  
	    byte b = (byte) "0123456789abcdef".indexOf(c);  
	    return b;
	}
	// 字节数组 --> 十六进制字符串
    public static String bin2Hex(byte[] bytes) {  
        String hexStr = "0123456789abcdef";
        String result = "";  
        String hex = "";  
        for (byte b : bytes) {  
            hex = String.valueOf(hexStr.charAt((b & 0xF0) >> 4));  
            hex += String.valueOf(hexStr.charAt(b & 0x0F));  
            result += hex;  
        }  
        return result;
    }
    
    
	
    /** -------------- 分隔线：数据处理 -------------- */
    
    // 去掉开始的连续0串
    public static String removeStartZeros(String str) {
    	int idx = 0;
    	for(char c: str.toCharArray()) {
    		if(c != '0')
    			break;
    		idx ++;
    	}
    	return str.substring(idx, str.length());
    }
    
	// 获取添加空格后的字符串
	public static String getStrWithSpace(String str){
		int len = str.length();
		String newStr = "";
		for (int i = 0; i < len; i += 2){
			newStr += str.substring(i, i+2) + " ";
		}
		return newStr;
	}
	// 获取去掉空格后的字符串
	public static String getStrWithOutSpace(String str){
		return str.replace(" ", "");
	}
	
	// 填充零字符串
	public static String fillZero(String str, int bytesLen){
		return getZeroStr(bytesLen * 2 - str.length()) + str;
	}
	public static String fillZeroAtTail(String str, int bytesLen) {
		return 	str + getZeroStr(bytesLen * 2 - str.length());
	}
	
    // 将整数转为二进制并倒序排列
	public static String getSign(int num){
		String str = Integer.toBinaryString(num);
		String newStr = "";
		
		int len = str.length();
		for(int i = 0; i < 32 - len; i ++){
			str = "0" + str;
		}
		str = new StringBuffer(str).reverse().toString();		
		for(int i = 0; i < 32; i += 4){
			newStr += str.substring(i, i+4) + " ";
			if(i == 4 || i == 12 || i == 20)
				newStr += " ";
		}
		return "[ " + newStr + "]";
	}

	// 把字符串格式的经纬度转为乘以10的6次方后的HexStr	 
	public static String handleLngLat(String lngOrLat){
		double doubleLngOrLat = Double.parseDouble(lngOrLat) * 1000000;
		return NormalTools.int2Hex((int)doubleLngOrLat, 4);
	} 

	// 判断value的第loc位是否为1
	public static boolean isFlagOne(int value, int loc){
		return (value & (1 << loc)) != 0;
	}

	
	/** -------------- 分割线：数据生成 -------------- */

	// 生成零字符串
	public static String getZeroStr(int len){
		String zeroStr = "";
		for(int i = 0; i < len; i ++)
			zeroStr += "0";
		return zeroStr;
	}

    // 生成指定范围的随机数（十六进制形式）
    public static String getRandomHex(int min, int max){
    	return int2Hex(getRandomInt(min, max));
    }
    public static int getRandomInt(int min, int max) {
    	return min + random.nextInt(max - min + 1);
    }
    
    // 生成指定长度的十六进制字符串
    public static String getRandomHexStr(int byteLen){
    	String hexStr = "0123456789abcdef";
		String rdStr = "";
		for(int i = 0; i < byteLen * 2; i ++){
			int loc = random.nextInt(16);
			rdStr += hexStr.substring(loc, loc + 1);
		}
		return rdStr;
	}
	
    // 生成YYMMDDhhmmss格式的时间
    public static String getTime(){
    	return sdf.format(new Date());
    }
    public static String getTime(int addSeconds) {
    	return sdf.format(new Date(System.currentTimeMillis() + addSeconds * 1000));
    }
    public static String getCanTime(){
    	return getTime().substring(6, 12) + "0000";
    }
    
    // 获取809UTC时间的秒数
    public static String get809UTCHex() {
    	return get809UTCHex(0);
    }
    public static String get809UTCHex(int addSeconds) {
    	return NormalTools.long2Hex(System.currentTimeMillis()/1000 - 3600 * 8 + addSeconds);
    }
    public static String getBjTimeFromUtcHex(String utcHex) {
    	Long seconds = Long.parseLong(NormalTools.getStrWithOutSpace(utcHex), 16);
    	Date date = new Date((seconds + 3600 * 8) * 1000);
    	return sdf1.format(date);
    }
    
	// 生成指定长度的数字字符串
	public static String getNumStr(int number, int totalLen){
		String str = Integer.toString(number);
		int count = totalLen - str.length();
		for (int i = 0; i < count; i ++){
			str = "0" + str;
		}
		return str;
	}
	
	// 生成rangdomInt的十六进制字符串
	public static String getHexRandomInt(){
		return getHexRandomInt(4);
	}
	public static String getHexRandomInt(int byteNum){
		return NormalTools.int2Hex(random.nextInt(), byteNum);
	}
	
	// 生成指定bit位为1的整数
	public static int pow(int num){
		return 1 << num;
	}	
	
	
	
	/** -------------- 分割线：文件操作 -------------- */
	
	// 重命名文件
	public static boolean renameFile(String oldFilePath, String newFileName){
		File oldFile = new File(oldFilePath);
		File newFile = new File(oldFile.getParent() + File.separator + newFileName);
		
		return oldFile.renameTo(newFile);
	}
	
	// 拷贝文件
	public static void copyFile(String oldFile, String newFile) throws Exception{
		FileInputStream fis = new FileInputStream(new File(oldFile));
		FileOutputStream fos = new FileOutputStream(new File(newFile));
		FileChannel inputChannel = fis.getChannel();
		FileChannel outputChannel = fos.getChannel();
		
		outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		
		fis.close();
		fos.close();
		inputChannel.close();
		outputChannel.close();
	}

	/** 文件转字节流 */
	public static String getHexStrFromFile(String fileName) {
		return NormalTools.bin2Hex(getBytesFromFile(fileName));
	}
    public static byte[] getBytesFromFile(String fileName){
    	FileInputStream in = null;
        ByteArrayOutputStream out = null;
        try {
        	URL url = NormalTools.class.getClassLoader().getResource(fileName);
            in = new FileInputStream(new File(url.getFile()));
            out = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            while (in.read(b) != -1) 
            	out.write(b, 0, b.length);
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }
	
	public static String getJsonByObject(Object obj) {
		return JSON.toJSONString(obj);
	}
	
	/** --------------- JSON处理 --------------- */
	// 将Map转为格式化JSON
	public static String getFormatJsonByMap(Map<?, ?> map) {
		return getFormatJson(JSON.toJSONString(map));
	}
	// JSON格式化
	public static String getFormatJson(String jsonStr){
		Object jsonObj = null;
		String formatJson = null;
		try {
			jsonObj = objMapper.readValue(jsonStr, Object.class);
			formatJson = objMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObj);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return formatJson;
	}
	
}
