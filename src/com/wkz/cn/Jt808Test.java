package com.wkz.cn;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import com.wkz.cn.jtparser.JtParserOf808;
import com.wkz.cn.protocol.ProtocolsOf808;
import com.wkz.cn.tools.NormalTools;

/** 
 * 
 * 报警标志【warnSign】 （2019版将预留的15-17三个位置用上，同时将第31位设为预留）
 * ================================================================================================
 * bit0-紧急  bit1-超速  bit2-疲劳  bit3-危险  bit4-GNSS模块故障  bit5-GNSS天线未接  bit6-GNSS天线短路
 * bit7-终端主电源欠压  bit8-终端主电源掉电  bit9-终端LCD或显示器故障
 * bit10-TTS模块故障  bit11-摄像头故障  bit12-IC卡模块故障  bit13-超速预警  bit14-疲劳预警  
 * bit15-违规行驶 bit16-胎压预警 bit17-右转盲区异常报警（2019版将这个3个预留字段用上了！！！！！！！！！！！！！！！！）
 * bit18-当天累计驾驶超时 bit19-超时停车  bit20-进出区域  bit21-进出路线   bit22-路段行驶时间不足/过长
 * bit23-路线偏移报警  bit24-车辆VSS故障  bit25-车辆油量异常  bit26-车辆被盗
 * bit27-车辆非法点火  bit28-车辆非法位移  bit29-碰撞预警  bit30-侧翻预警  
 * bit31-非法开门报警（2019版将第31位设置为预留！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！）
 * 
 * 车辆状态【status】（2019版将预留的6-7和22位用上了）
 * ==============================================================================================
 * bit6-紧急刹车系统采集的前撞预警 bit7-车道偏离预警（2019版将6-7的预留位用上了！！！！！！！！！！！！！！！！！！！！）
 * bit22-车辆行驶状态（2019版将预留的22-31中的第22位用上了，预留范围变为23-31！！！！！！！！！！！！！！！！！！！！）
 * 
  * 位置附加信息【locAppendInfo】（2019版将预留附加信息ID-0x05和0x06用上了）
 * 0x05-胎压 0x06-车厢温度
 * 
 * 
  * 位置附件信息——报警相关
 * =================================================================================================
 * 【0x11】超速报警信息，1或5字节(区域ID占4个字节)：0无：1圆形：2矩形：3多边形：4路线
 * 【0x12】进出区域报警信息，BYTE[6](6字节)：0进1出 || 1圆形：2矩形：3多边形：4路线
 * 【0x13】路段行驶时间不足/过长报警，BYTE[7](7字节)：路段ID || 行驶时间 || 0不足1过长
 * ------------------------------------------------------------------------------------
 * 【0x14】视频相关报警，DWORD(4字节)
 *		bit0信号丢失{0x15}
 *		bit1信号遮挡{0x16}
 *		bit2存储器故障{0x17}
 *		bit3其他设备故障
 *		bit4客车超员
 *		bit5异常驾驶{0x18}
 *		bit6录像存储达到阈值
 */	

public class Jt808Test {	
	public static void main(String[] args) throws Exception {
		Socket socket = new Socket("127.0.0.1", 12345);
		String teminalTel = "18866668888";
		
		String authCode = sendRegisterInfo(socket, teminalTel);
		test(socket, ProtocolsOf808.get0x0102Data(teminalTel, authCode));
		for(int i = 0; i < 0; i ++) {
			sendData(socket, ProtocolsOf808.get0x0200Data(teminalTel, 1, 7));
			System.out.println("--------------------------------------------------\n");
			//Thread.sleep(1000);
		}
		socket.close();
		
//		// 1、【0x0100】注册信息发送
//		String authCode = sendRegisterInfo(socket, teminalTel);
//		
//		// 2、【0x0102】鉴权信息发送
//		test(socket, ProtocolsOf808.get0x0102Data(teminalTel, authCode));
//		
//		// 3、【0x0200】位置信息发送
//		test(socket, ProtocolsOf808.get0x0200Data(teminalTel, 1, 7));
//		
//		// 4、【0x0608】查询区域或路线应答消息
//		test(socket, ProtocolsOf808.get0x0608Data(teminalTel));
//		
//		// 驾驶员身份信息采集上报
//		test(socket, ProtocolsOf808.get0x0702Data(teminalTel));
//		
//		// 摄像头立即拍摄应答消息
//		test(socket, ProtocolsOf808.get0x0805Data(teminalTel));
//		
//		// 查询区域或路线数据应答
//		test(socket, ProtocolsOf808.get0x0608Data(teminalTel, 3, 3));
		
		
//		List<String> datas = ProtocolsOf808.get0x0801Data(teminalTel);
//		
//		OutputStream os = socket.getOutputStream();
//		InputStream is = socket.getInputStream();
//
//		for(String data: datas) {
//			os.write(NormalTools.hex2Bin(data));
//			os.flush();
//			Thread.sleep(100);
//		}
//				
//		byte[] buff = new byte[256];
//		is.read(buff);
//		Thread.sleep(10);
//		String str = NormalTools.bin2Hex(buff);
//		System.out.println(str.substring(0, str.lastIndexOf("7e") + 2));
//		
//		socket.close();
	}
	
	
	// 【0x0100】注册信息发送
	public static String sendRegisterInfo(Socket socket, String teminalTel) throws Exception{
		String regData = ProtocolsOf808.get0x0100Data(teminalTel, "京F88888");
		System.out.println("注册发送：" + regData);
		String regRespData = sendData(socket, regData);
		System.out.println("注册返回：" + regRespData);
		String authCode = regRespData.split("7e8100")[1].substring(26, 48);
		System.out.println("注册鉴权码：" + authCode);
		return authCode;
	}

	
	// 【协议发送与接收】
	public static void test(Socket socket, String data) throws Exception{
		test(socket, data, "");
	}
	public static void test(Socket socket, String data, String desc) throws Exception{
		System.out.println(desc + "发送：" + data);
		System.out.println("-----------------------------------------------------");
		System.out.println(JtParserOf808.getFormatMsg(data));
		System.out.println("-----------------------------------------------------");
		String recvData = sendData(socket, data);
		System.out.println(desc + "接收：" + recvData);
		System.out.println("-----------------------------------------------------");
		System.out.println(JtParserOf808.getFormatMsg(recvData));
	}
	
	// 向指定socket发送数据
	private static String sendData(Socket socket, String data) throws Exception{
		OutputStream os = socket.getOutputStream();
		InputStream is = socket.getInputStream();
		byte[] buff = new byte[256];
		os.write(NormalTools.hex2Bin(data));
		os.flush();
		Thread.sleep(10);
		is.read(buff);
		Thread.sleep(10);
		String str = NormalTools.bin2Hex(buff);
		return str.substring(0, str.lastIndexOf("7e") + 2);		// 用于接收数据的buff固定长度为256
	}
}
